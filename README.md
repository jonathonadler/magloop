# Installation 

### Single line installation (requires root) 

- Script will prompt for root password

```bash
$ wget -O - https://bitbucket.org/jonathonadler/magloop/raw/master/scripts/install.sh | sudo /bin/bash
```

### Enable the I2C port (Raspian) 

See: https://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/

### Grant access to USB ports (when not running as a super user)  

```bash
$ sudo usermod -a -G dialout $(logname)
```

# Usage
```bash
$ magloop --help
```
```text
usage: magloop.py [-h] [--show-rigs] [--log-cfg LOG_CFG] [--debug] [--trace]
                  [--database-uri DATABASE_URI]
                  [--config-profile-id {1,2,3,4,5,6,7,8,9,10}] [--ui]
                  [--ui-lcd-i2c-expander {PCF8574,MCP23008,MCP23017}]
                  [--ui-lcd-address {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f}]
                  [--ui-lcd-cols UI_LCD_COLS] [--ui-lcd-rows UI_LCD_ROWS]
                  [--ui-lcd-charmap {A00,A02}]
                  [--ui-lcd-backlight-timeout UI_LCD_BACKLIGHT_TIMEOUT]
                  [--ui-joy-address {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f}]
                  [--ui-joy-z-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper] [--stepper-driver {GENERIC,MICROSTEP}]
                  [--stepper-dir-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper-pul-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper-micro-step {1,2,4,8,16,32}]
                  [--stepper-ena-offline {0,1}]
                  [--stepper-ena-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper-ena-delay STEPPER_ENA_DELAY]
                  [--stepper-micro-step-select1-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper-micro-step-select2-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--stepper-micro-step-select3-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}]
                  [--rig] [--rig-model-id RIG_MODEL_ID]
                  [--rig-pathname RIG_PATHNAME] [--rig-timeout RIG_TIMEOUT]
                  [--rig-retry RIG_RETRY]
                  [--rig-ptt-type {RIG,DTR,RTS,Parallel,CM108,GPIO,GPION,None}]
                  [--rig-ptt-pathname RIG_PTT_PATHNAME]
                  [--rig-ptt-bitnum RIG_PTT_BITNUM]
                  [--rig-serial-speed {75,110,300,1200,2400,4800,9600,19200,38400,57600,115200}]
                  [--rig-serial-data-bits {5,6,7,8}]
                  [--rig-serial-stop-bits {0,1}]
                  [--rig-serial-parity {None,Odd,Even,Mark,Space}]
                  [--rig-serial-handshake {None,XONXOFF,Hardware}]
                  [--rig-serial-rts-state {Unset,ON,OFF}]
                  [--rig-serial-dtr-state {Unset,ON,OFF}] [--webapp]
                  [--webapp-address WEBAPP_ADDRESS]
                  [--webapp-port WEBAPP_PORT]
                  [@<filename>]

Magnetic Loop Antenna Controller

positional arguments:
  @<filename>           read command line arguments from file

optional arguments:
  -h, --help            show this help message and exit

commands:
  --show-rigs           show available Hamlib rigs and exit

logging:
  --log-cfg LOG_CFG     log configuration file in YAML format (default:
                        /opt/magloop/magloop/config/logging.yaml)
  --debug               enable debug mode
  --trace               enable trace mode

database:
  --database-uri DATABASE_URI
                        database connection string (default:
                        sqlite:////home/jonathonadler/magloop.db)

configuration profile:
  --config-profile-id {1,2,3,4,5,6,7,8,9,10}
                        configuration profile id (default: 1)

ui:
  --ui                  enable the user interface
  --ui-lcd-i2c-expander {PCF8574,MCP23008,MCP23017}
                        lcd I2C expander (port type)
  --ui-lcd-address {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f}
                        lcd I2C address
  --ui-lcd-cols UI_LCD_COLS
                        lcd columns
  --ui-lcd-rows UI_LCD_ROWS
                        lcd rows
  --ui-lcd-charmap {A00,A02}
                        lcd charmap
  --ui-lcd-backlight-timeout UI_LCD_BACKLIGHT_TIMEOUT
                        lcd backlight timeout (s) [-1=timeout]
  --ui-joy-address {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f}
                        joystick I2C address
  --ui-joy-z-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        joystick z button GPIO

stepper:
  --stepper             enable the stepper motor driver
  --stepper-driver {GENERIC,MICROSTEP}
                        stepper driver type. Select GENERIC if microstep
                        setting is hardware controlled. Select MICROSTEP if
                        microstep setting is contolled via GPIO (requires
                        --stepper-micro-step-modeX)
  --stepper-dir-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver direction GPIO
  --stepper-pul-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver pulse GPIO
  --stepper-micro-step {1,2,4,8,16,32}
                        stepper microstep setting (number of pulses per full
                        step)
  --stepper-ena-offline {0,1}
                        stepper driver enable offline mode (0=Low, 1=High).
                        Select 0 (Low) for TB6600 driver. Select 1 (High) for
                        DRV8825/A4988 etc.
  --stepper-ena-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver enable offline GPIO
  --stepper-ena-delay STEPPER_ENA_DELAY
                        stepper driver enable offline delay (ms) [-1=no
                        release]
  --stepper-micro-step-select1-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver microstep select pin 1 GPIO [MICROSTEP
                        stepper driver only]
  --stepper-micro-step-select2-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver microstep select pin 2 GPIO [MICROSTEP
                        stepper driver only]
  --stepper-micro-step-select3-gpio {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}
                        stepper driver microstep select pin 3 GPIO [MICROSTEP
                        stepper driver only]

rig:
  --rig                 enable hamlib rig
  --rig-model-id RIG_MODEL_ID
                        Hamlib rig model ID
  --rig-pathname RIG_PATHNAME
                        Hamlib path name to the libs.device file of the rig
  --rig-timeout RIG_TIMEOUT
                        Hamlib timeout (ms)
  --rig-retry RIG_RETRY
                        Hamlib retry
  --rig-ptt-type {RIG,DTR,RTS,Parallel,CM108,GPIO,GPION,None}
                        Hamlib Push-To-Talk interface type
  --rig-ptt-pathname RIG_PTT_PATHNAME
                        Hamlib path name to the libs.device file of the Push-
                        To-Talk
  --rig-ptt-bitnum RIG_PTT_BITNUM
                        Hamlib Push-To-Talk Bit number for CM108 GPIO
  --rig-serial-speed {75,110,300,1200,2400,4800,9600,19200,38400,57600,115200}
                        Hamlib serial speed
  --rig-serial-data-bits {5,6,7,8}
                        Hamlib serial data bits
  --rig-serial-stop-bits {0,1}
                        Hamlib serial stop bits
  --rig-serial-parity {None,Odd,Even,Mark,Space}
                        Hamlib serial parity
  --rig-serial-handshake {None,XONXOFF,Hardware}
                        Hamlib serial handshake
  --rig-serial-rts-state {Unset,ON,OFF}
                        Hamlib RTS state
  --rig-serial-dtr-state {Unset,ON,OFF}
                        Hamlib DTR state

webapp:
  --webapp              enable the HTTP server
  --webapp-address WEBAPP_ADDRESS
                        HTTP listen address
  --webapp-port WEBAPP_PORT
                        HTTP listen port

Stepper motors typically have a step size specification (e.g. 1.8° or 200
steps per revolution), which applies to full steps. A microstepping driver
such as the A4988 or DRV8825 allows higher resolutions by allowing
intermediate step locations, which are achieved by energizing the coils with
intermediate current levels. For instance, driving a motor in quarter-step
mode will give the 200-step-per-revolution motor 800 microsteps per revolution
by using four different current levels. For further information, see:
https://www.pololu.com/product/1183 and https://www.pololu.com/product/2133```

# Show all supported rigs 

```bash
$ magloop --show-rigs
```

```text
+----------------+------------------------+------------------------+------------+----------+
|   Rig Model ID | Manufacturer           | Model                  | Version    | Status   |
|----------------+------------------------+------------------------+------------+----------|
|              1 | Dummy                  | Hamlib                 | 0.5        | Beta     |
|              2 | NET rigctl             | Hamlib                 | 1.0        | Stable   |
|              4 | FLRig                  | FLRig                  | 1.5        | Stable   |
...
...
...
|            231 | TS-590S                | Kenwood                | 1.1.1      | Beta     |
...
...
...
|           3102 | DRA818U                | Dorji                  | 0.1        | Untested |
|           3201 | 2050                   | Barrett                | 0.2        | Beta     |
|           3301 | FDM-DUO                | ELAD                   | 1.0.5      | Untested |
+----------------+------------------------+------------------------+------------+----------+
```

# Examples

### Kenwood TS-590S connected directly to Raspberry Pi via USB. 
```bash
$ magloop --ui --stepper --stepper-micro-step 2 --rig --rig-model-id 231 --rig-path /dev/ttyUSB0
```

### Remote control of Kenwood TS-590S via Netrig. In this example the Rig is connected to an iMac (host) via USB.
On iMac (host)
```bash
$ hostname
iMac.local
 
$ rigctld -m 231 -r /dev/tty.SLAB_USBtoUART
```
On Raspberry Pi 
```bash
$ magloop --ui --stepper --stepper-micro-step 2 --rig --rig-model-id 2 --rig-path iMac.local:4532
```

# Parts

* [RaspberryPi Model 3](https://duckduckgo.com/?q=raspberry+pi+model+3&t=ffsb&iar=images&iax=images&ia=images)
* [RaspberryPi Breakout board](https://duckduckgo.com/?q=raspberry+pi+breakout+board&t=ffsb&iar=images&iax=images&ia=images)
* [2004 I2C LCD (20*4 Characters)](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20190302024630&SearchText=lcd+display+i2c+2004)
* [Analog Joystick Module](https://duckduckgo.com/?q=analog+joystick+module+raspberry+pi&t=ffsb&iar=images&iax=images&ia=images)
* [PCF8591 AD-DA I2C Module](https://duckduckgo.com/?q=PCF8591+AD-DA+I2C+Module&t=ffsb&iar=images&iax=images&ia=images)

### Wiring (Breadboard)
![Breadboard](./docs/magloop_breadboard.png "Breadboard")

# Useful Information

### Hardware

* [HD44780U (LCD-II) Dot Matrix Liquid Crystal Display Controller/Driver](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf)
* [PCF8591 AD Converter](https://www.sunfounder.com/learn/sensor-kit-v2-0-for-b/lesson-13-pcf8591-ad-converter-sensor-kit-v2-0-for-b.html)
* [Breakout board for A4988 Stepper Motor Driver](https://forum.hobbycomponents.com/viewtopic.php?t=2159)
* [A4988 Stepper Motor Driver Carrier with Voltage Regulators](https://www.pololu.com/product/1183)
* [DRV8825 Stepper Motor Driver Carrier, High Current](https://www.pololu.com/product/2133)

### Other

* [The comprehensive GPIO Pinout guide for the Raspberry Pi]([https://pinout.xyz/)
* [pigpio is a library for the Raspberry which allows control of the General Purpose Input Outputs](http://abyz.me.uk/rpi/pigpio/)
* [How to share a terminal session using screen](http://wiki.networksecuritytoolkit.org/index.php/HowTo_Share_A_Terminal_Session_Using_Screen)
* [Fritzing is an open-source hardware initiative that makes electronics accessible as a creative material for anyone](http://fritzing.org/home/)

# TODO

* add variable capacitor mounts to repo
* add pully details