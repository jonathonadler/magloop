#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# the temp directory used
TEMP_DIR=`mktemp -d`

# check if tmp dir was created
if [[ ! "${TEMP_DIR}" || ! -d "${TEMP_DIR}" ]]; then
  echo "Could not create temp dir"
  exit 1
fi

# deletes the temp directory
cleanup () {
  rm -rf "${TEMP_DIR}"
  echo "Deleted temp working directory ${TEMP_DIR}"
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

# Make Silicon Labs CP2102 USB to UART Bridge Controller driver - Used by Kenwood TS-590s
apt-get update && apt-get -y install raspberrypi-kernel-headers

# if kernel version has changed since last reboot, then make will fail. Solution: reboot before make.
echo "Kernel Version:" $(uname -r)

echo "USB Devices:"
usb-devices

# make USB driver
pushd ${TEMP_DIR} >/dev/null
echo "Downloading."
wget https://www.silabs.com/documents/login/software/Linux_3.x.x_4.x.x_VCP_Driver_Source.zip
echo "Extracting."
unzip Linux_3.x.x_4.x.x_VCP_Driver_Source.zip
tar -xvzf Linux_3.x.x_4.x.x_VCP_Driver_Source.tar.gz
echo "Building."
make
# install driver
echo "Installing."
cp cp210x.ko /lib/modules/$(uname -r)/kernel/drivers/usb/serial
rmmod cp210x
rmmod usbserial
insmod /lib/modules/$(uname -r)/kernel/drivers/usb/serial/usbserial.ko
insmod /lib/modules/$(uname -r)/kernel/drivers/usb/serial/cp210x.ko
popd >/dev/null

# grant read/write access to USB devices to current user
usermod -a -G dialout $(logname)