#!/usr/bin/env bash
SCRIPTPATH="$(dirname "$(readlink -f "$0")")"
source ${SCRIPTPATH}/../env/bin/activate
python ${SCRIPTPATH}/../magloop/magloop.py $@