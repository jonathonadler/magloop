#!/usr/bin/env bash

#set -o xtrace # trace what gets executed
set -o errexit # exit when a command fails.

if [ $EUID -ne 0 ]; then
   echo "This script must be run as root"
   exit 1
fi

set -o nounset # exit when your script tries to use undeclared variables
set -o pipefail # exit when prevent piping causes non-zero exit codes

# the temp directory used
TEMP_DIR=`mktemp -d`

# check if tmp dir was created
if [ ! "${TEMP_DIR}" ] || [ ! -d "${TEMP_DIR}" ]; then
  echo "Could not create temp dir"
  exit 1
fi

# deletes the temp directory
cleanup () {
  rm -rf "${TEMP_DIR}"
  echo "Deleted temp working directory ${TEMP_DIR}"
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

apt_update () {
    echo "Retrieve new lists of packages"
    apt-get update
}

install_fishshell () {
    echo "Installing fish shell packages"
    apt-get -y install fish-common
}

install_samba () {
    echo "Installing samba packages"
    apt-get -y install samba samba-common-bin
}

create_samba_share () {
    local SAMBA_SHARE=magloop
    local SAMBA_SHARE_PATH=/opt/magloop

    if ! grep -q "\[${SAMBA_SHARE}\]" /etc/samba/smb.conf; then
        echo "Creating samba share: ${SAMBA_SHARE_PATH}"

        # share new user home directory
        cat >> /etc/samba/smb.conf << EOT

[${SAMBA_SHARE}]
Comment = ${SAMBA_SHARE} folder
Path = ${SAMBA_SHARE_PATH}
Browseable = yes
Writeable = Yes
only guest = no
create mask = 0777
directory mask = 0777
Public = yes
Guest ok = yes
EOT

        # restart sambda for change to take effect
        /etc/init.d/samba restart
    fi
}

apt_update
install_fishshell
install_samba
create_samba_share
