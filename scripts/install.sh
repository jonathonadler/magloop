#!/usr/bin/env bash

#set -o xtrace # trace what gets executed
set -o errexit # exit when a command fails.

if [ $EUID -ne 0 ]; then
   echo "This script must be run as root"
   exit 1
fi

set -o nounset # exit when your script tries to use undeclared variables
set -o pipefail # exit when prevent piping causes non-zero exit codes

# the temp directory used
TEMP_DIR=`mktemp -d`

# check if tmp dir was created
if [ ! "${TEMP_DIR}" ] || [ ! -d "${TEMP_DIR}" ]; then
  echo "Could not create temp dir"
  exit 1
fi

# deletes the temp directory
cleanup () {
  rm -rf "${TEMP_DIR}"
  echo "Deleted temp working directory ${TEMP_DIR}"
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

apt_update () {
    echo "Retrieve new lists of packages"
    apt-get update
}

install_build_tools () {
    echo "Installing build tools packages"
    apt-get -y install git-core build-essential autoconf automake libtool texinfo python-dev swig
}

install_python_extras () {
    echo "Installing python extra packages"
    apt-get -y install python3-pip virtualenv libffi-dev
}

install_pigpio () {
    echo "Installing pigpio packages"
    apt-get -y install pigpio python3-pigpio
    systemctl enable pigpiod
    systemctl start pigpiod
}

install_hamlib () {
    # To get Python 3 bindings, we must compile from source, so we cannot install distro package.
    #apt-get -y install libhamlib-utils

    local RIGCTL=$(which rigctl)

    if [ -x "${RIGCTL}" ] ; then
        echo "hamlib already exists: ${RIGCTL}"
        return
    fi

    echo "Installing hamlib (compiling from source)"

    # clone Hamlib source
    pushd ${TEMP_DIR} >/dev/null
    git clone https://github.com/Hamlib/Hamlib.git && cd Hamlib

    # set python v3
    export PYTHON=$(which python3)

    # make and install from source
    autoreconf --install
    ./configure --with-python-binding
    make
    make install

    # update Dynamic Linker Run Time Bindings
    ldconfig
    popd >/dev/null
}

install_magloop () {
    echo "Installing Magnetic Loop Controller"

    rm -rf ${INSTALL_DIR}
    git clone https://jonathonadler@bitbucket.org/jonathonadler/magloop.git ${INSTALL_DIR}

    # set permissions
    chmod g+s ${INSTALL_DIR}
    chmod -R ug=rwx,o=rx ${INSTALL_DIR}
    chmod a+x ${INSTALL_DIR}/scripts/*.sh

    # make accessible to magloop group
    groupadd -f  magloop
    chown -R :magloop ${INSTALL_DIR}

    # create symlink in path
    ln -sf ${INSTALL_DIR}/scripts/magloop.sh /usr/local/bin/magloop
}

create_virtualenv () {
    local VIRTUALENV=${INSTALL_DIR}/env

    echo "Creating virtual environment: ${VIRTUALENV}"

    # create new virtual environment
    virtualenv -p python3 ${VIRTUALENV}

    # activate virtual environment
    set +o nounset ; source ${VIRTUALENV}/bin/activate

    # install packages into virtual environment
    pip install -r ${INSTALL_DIR}/requirements.txt

    # add python /usr/local/lib site packages to virtual environment (for Hamlib)
    echo "/usr/local/lib/python3.5/site-packages/" > ${VIRTUAL_ENV}/lib/python3.5/site-packages/usrlocallib.pth
}

install_webserver () {
    apt-get -y install nginx
    rm -f /etc/nginx/sites-enabled/default
    # link to magloop nginx configuration
    ln -sf ${INSTALL_DIR}/web/conf/magloop.nginx /etc/nginx/sites-enabled/magloop
    systemctl restart nginx.service
}

INSTALL_DIR=/opt/magloop

apt_update
install_build_tools
install_python_extras
install_pigpio
install_hamlib
install_magloop
create_virtualenv
install_webserver