########################################################################
# Filename    : application.py
# Description : Main functions for magnetic loop application
########################################################################

import logging
import signal
import sys

import gevent
from connexion import FlaskApp
from flask import g
from flask_cors import CORS
from gevent.event import Event

import config
import config.enum as enums
from apps.magloop.menu import MenuDateTime
from apps.magloop.menu import MenuLanIp
from apps.magloop.menu import MenuReboot
from apps.magloop.menu import MenuRigFrequency
from apps.magloop.menu import MenuRigInfo
from apps.magloop.menu import MenuRigMode
from apps.magloop.menu import MenuRigPTT
from apps.magloop.menu import MenuRigPower
from apps.magloop.menu import MenuRigVfo
from apps.magloop.menu import MenuShutdown
from apps.magloop.menu import MenuStepperHome
from apps.magloop.menu import MenuStepperInfo
from apps.magloop.menu import MenuStepperMove
from apps.magloop.menu import MenuSystemInfo
from libs.device.joystick import JoystickPCF8591
from libs.device.lcd import CharLCD
from libs.device.rig import Rig
from libs.device.stepper import StepperGeneric, StepperMicrostep
from libs.logging import setup_app_logging
from libs.ui.menu import MenuNav
from libs.ui.virtualui import VirtualUI, MenuRunner
from libs.webapp import WebApp
from models import db, Settings, User, ConfigurationProfile
from models.config_profile import CONFIG_PROFILE_SECTIONS
from schemas import ma

_logger = logging.getLogger(__name__)


class ConnexionApp(FlaskApp):
    def create_api(self, specification, **kwargs):
        api = super(FlaskApp, self).add_api(specification, **kwargs)
        return api

    def register_api(self, api):
        self.app.register_blueprint(api.blueprint)


def log_model(model, level=logging.INFO):
    """
    log each non-primary key and non-foreign key column value for an SQLAlchemy model
    :param model: db model
    :param level: log level
    """
    for c in [c for c in model.__table__.columns if not c.primary_key and len(c.foreign_keys) == 0]:
        _logger.log(level=level, msg='{}.{}: {}'.format(model.__table__, c.name, getattr(model, c.name)))


def log_config_profile(config_profile, level=logging.INFO):
    """
    log configuration profile
    :param config_profile: configuration profile
    :param level: log level
    """
    log_model(config_profile, level)
    for config_profile_section in CONFIG_PROFILE_SECTIONS:
        log_model(getattr(config_profile, config_profile_section), level)


def create_connexion_app(debug):
    """
    create connexion/flask app
    :param debug: Debug Mode?
    :return: connexion/flask app
    """
    options = {"swagger_ui": True}
    connexion_app = ConnexionApp(__name__, specification_dir='../api/', options=options)
    if debug:
        # flask configuration
        connexion_app.app.config['DEBUG'] = True
        # enable CORS
        CORS(connexion_app.app)

    return connexion_app


def init_db(connexion_app, database_uri, debug):
    """
    init SQLalchemy db
    :param connexion_app: connexion/flask app
    :param database_uri: SQLAlchemy database URI
    :param debug: Debug Mode?
    """
    # flask-sqlalchemy configuration
    connexion_app.app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
    connexion_app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.SQLALCHEMY_TRACK_MODIFICATIONS
    if debug:
        # sqlalchemy configuration
        logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

    # Initialize SQLAlchemy
    db.init_app(connexion_app.app)
    # Order matters: Initialize SQLAlchemy before Marshmallow
    ma.init_app(connexion_app.app)


def create_db():
    """
    create database if required
    """
    db.create_all()

    settings = Settings.get()
    if not settings:
        settings = Settings()
        settings.save()

    admin = User.get_user_by_username('admin')
    if not admin:
        admin = User(username='admin', password='admin', full_name='System Administrator', is_admin=True)
        admin.save()

    for i in range(1, 11):
        profile = ConfigurationProfile.get_one_profile(i)

        if not profile:
            profile = ConfigurationProfile()

            profile.is_active = True
            profile.ui.enabled = True
            profile.ui.lcd_i2c_expander = config.UI_LCD_I2C_EXPANDER
            profile.ui.lcd_address = config.UI_LCD_ADDRESS
            profile.ui.lcd_cols = config.UI_LCD_COLS
            profile.ui.lcd_rows = config.UI_LCD_ROWS
            profile.ui.lcd_charmap = config.UI_LCD_CHARMAP
            profile.ui.lcd_backlight_timeout = config.UI_LCD_BACKLIGHT_TIMEOUT
            profile.ui.joy_address = config.UI_JOYSTICK_ADDRESS
            profile.ui.joy_z_gpio = config.UI_JOYSTICK_Z_GPIO
            profile.stepper.enabled = True
            profile.stepper.driver = config.STEPPER_DRIVER
            profile.stepper.dir_gpio = config.STEPPER_DIR_GPIO
            profile.stepper.pul_gpio = config.STEPPER_PUL_GPIO
            profile.stepper.micro_step = config.STEPPER_MICRO_STEP
            profile.stepper.ena_offline = config.STEPPER_ENA_OFFLINE
            profile.stepper.ena_gpio = config.STEPPER_ENA_GPIO
            profile.stepper.ena_delay = config.STEPPER_ENA_DELAY_MS
            profile.stepper.micro_step_select1_gpio = config.STEPPER_MICRO_STEP_SELECT1_GPIO
            profile.stepper.micro_step_select2_gpio = config.STEPPER_MICRO_STEP_SELECT2_GPIO
            profile.stepper.micro_step_select3_gpio = config.STEPPER_MICRO_STEP_SELECT3_GPIO
            profile.rig.enabled = True
            profile.rig.model_id = config.RIG_MODEL_ID
            profile.rig.timeout = config.RIG_TIMEOUT_MS
            profile.rig.retry = config.RIG_RETRY
            profile.webapp.enabled = True
            profile.webapp.address = config.WEBAPP_ADDRESS
            profile.webapp.port = config.WEBAPP_PORT

            profile.save()

            _logger.info('default configuration profile created: {}'.format(profile))


def load_settings():
    """
    load settings from database
    """
    _logger.info('loading settings')

    return Settings.get()


def load_config_profile(config_profile_id=None):
    """
    load configuration profile from database
    :param config_profile_id: configuration profile id, if not specified - get active configuration profile
    :return: configuration profile [model.ConfigurationProfile]
    """
    if config_profile_id:
        config_profile = ConfigurationProfile.get_one_profile(config_profile_id)
    else:
        config_profile = ConfigurationProfile.get_active_profile()

    return config_profile


# map args values (by argument group) into configuration profile section
def map_cmdline_args_to_config_profile(args, config_profile):
    """
    Map command line args by arg group to corresponding configuration profile section.

    e.g.
        args.rig.enabled -> config_profile.rig.enabled

    :param args: argparse args fro map from - usually from parser.parse_args()
    :param config_profile: the configuration profile to map into
    """
    for arg_group in CONFIG_PROFILE_SECTIONS:  # list of "sections" within a configuration profile. eg. rig, stepper etc.
        config_profile_section = getattr(config_profile, arg_group)

        # set configuration profile enabled attribute
        if getattr(args, arg_group) and not config_profile_section.enabled:
            config_profile_section.enabled = True

            _logger.info('Applying command line override - {}.{}: {}'
                         .format(config_profile_section.__table__,
                                 'enabled',
                                 config_profile_section.enabled))

        # set configuration profile section attributes
        arg_prefix = arg_group + '_'
        filtered_arg = {k: v for k, v in vars(args).items() if k.startswith(arg_prefix) and v}
        for k, v in filtered_arg.items():
            attr_name = k[len(arg_prefix):]
            if hasattr(config_profile_section, attr_name):
                if v != getattr(config_profile_section, attr_name):
                    setattr(config_profile_section, attr_name, v)

                    _logger.info('Applying command line override - {}.{}: {}'
                                 .format(config_profile_section.__table__,
                                         attr_name,
                                         getattr(config_profile_section, attr_name)))


def create_stepper(config_profile):
    try:
        if config_profile.stepper.driver == enums.StepperDriver.GENERIC.value:
            stepper = StepperGeneric(**vars(config_profile.stepper))
        elif config_profile.stepper.driver == enums.StepperDriver.MICROSTEP.value:
            stepper = StepperMicrostep(**vars(config_profile.stepper))
        else:
            raise ValueError('Invalid stepper driver: {}'.format(config_profile.stepper.driver))

        _logger.info('created stepper: {}'.format(stepper))
        return stepper
    except:
        _logger.exception('errror creating stepper')


def create_rig(config_profile):
    try:
        rig = Rig(**vars(config_profile.rig))
        _logger.info('created rig: {}'.format(rig))
        return rig
    except:
        _logger.exception('errror creating rig')


def create_webapp(config_profile, connexion_app):
    try:
        webapp = WebApp(application=connexion_app, **vars(config_profile.webapp))
        _logger.info('created webapp: {}'.format(webapp))
        return webapp
    except:
        _logger.exception('errror creating webapp')


def create_virtualui(config_profile):
    try:
        virtualui = VirtualUI(
            CharLCD(i2c_expander=config_profile.ui.lcd_i2c_expander,
                    address=int(config_profile.ui.lcd_address, 16),
                    cols=config_profile.ui.lcd_cols,
                    rows=config_profile.ui.lcd_rows,
                    charmap=config_profile.ui.lcd_charmap,
                    backlight_timeout=config_profile.ui.lcd_backlight_timeout),
            JoystickPCF8591(port=1,
                            address=int(config_profile.ui.joy_address, 16),
                            z_gpio=config_profile.ui.joy_z_gpio)
        )

        _logger.info('created virtualui: {}'.format(virtualui))
        return virtualui
    except:
        _logger.exception('errror creating virtualui')


def create_menurunner(virtualui, rig, stepper):
    try:
        menuitems = []

        if rig:
            rig_menuitems = [MenuRigInfo(title='Rig Info', rig=rig),
                             MenuRigFrequency(title='Frequency', rig=rig),
                             MenuRigPower(title='Power', rig=rig),
                             MenuRigVfo(title='VFO', rig=rig),
                             MenuRigMode(title='Mode', rig=rig),
                             MenuRigPTT(title='Push To Talk', rig=rig)]
            menuitems.append(MenuNav(title='Rig', menuitems=rig_menuitems))

        if stepper:
            stepper_menuitems = [MenuStepperInfo(title='Stepper Info', stepper=stepper),
                                 MenuStepperMove(title='Move', stepper=stepper),
                                 MenuStepperHome(title='Home', stepper=stepper, confirmation_title='Home Stepper')]
            menuitems.append(MenuNav(title='Stepper', menuitems=stepper_menuitems))

        menuitems.append(MenuSystemInfo(title='System Info'))
        menuitems.append(MenuDateTime(title='Date/Time'))
        menuitems.append(MenuLanIp(title='Network'))
        menuitems.append(MenuReboot(title='Reboot RPi'))
        menuitems.append(MenuShutdown(title='Shutdown RPi'))

        top_menu = MenuNav(title='Menu', menuitems=menuitems, cols=virtualui.charlcd.lcd.cols,
                           rows=virtualui.charlcd.lcd.rows)

        menurunner = MenuRunner(virtualui, top_menu)
        _logger.info('created menurunner: {}'.format(menurunner))
        return menurunner
    except:
        _logger.exception('errror creating menurunner')


class MagLoopApplication():
    def __init__(self, daemon=None, log_cfg=None, debug=None, trace=None, database_uri=None, *args, **kwargs):
        self.daemon = daemon
        self.log_cfg = log_cfg
        self.debug = debug
        self.trace = trace
        self.database_uri = database_uri

        self.connexion_app = None
        self.settings = None
        self.stepper = None
        self.rig = None
        self.webapp = None
        self.menurunner = None

        self._stop_event = Event()

    def init_app(self, args):
        try:
            self._init_app(args)
        except Exception as ex:
            _logger.exception('error during program initialisation, exiting...')
            sys.exit(1)

    def _init_app(self, args):
        # initialise logging
        if self.trace:
            default_level = logging.TRACE
        elif self.debug:
            default_level = logging.DEBUG
        else:
            default_level = logging.INFO

        setup_app_logging(log_cfg_yaml_path=self.log_cfg, default_level=default_level)

        _logger.info('initialising application...')

        if self.trace:
            _logger.info('trace mode enabled')
            self.debug = True
        elif self.debug:
            _logger.info('debug mode enabled')

        # initialise connexion/flask app and init SQLalchemy db
        self.connexion_app = create_connexion_app(self.debug)

        init_db(self.connexion_app, self.database_uri, self.debug)

        _logger.debug('connexion_app.app.config: {}'.format(self.connexion_app.app.config))

        with self.connexion_app.app.app_context():
            # create database if required
            create_db()

            # load settings from database and override config object
            self.settings = load_settings()
            config.JWT_SECRET = self.settings.jwt_secret

            # load configuration profile
            try:
                config_profile_id = args.config_profile_id
            except:
                config_profile_id = None

            # load configuration profile from db
            config_profile = load_config_profile(config_profile_id)
            _logger.info('configuration profile loaded: {}'.format(config_profile.id))

            # ensure configuration profile is active
            config_profile.is_active = True
            _logger.info('configuration profile set active: {}'.format(config_profile.id))

            # map args values (by argument group) into configuration profile (section)
            if args:
                map_cmdline_args_to_config_profile(args, config_profile)

            config_profile.update()
            _logger.info('configuration profile saved: {}'.format(config_profile.id))

            # log configuration profile
            log_config_profile(config_profile)

        # create blueprint
        api = self.connexion_app.create_api('magloop.yaml', base_path='/api', validate_responses=self.debug)

        @api.blueprint.before_request
        def append_globals():
            # ensure that global variables are present within context of blueprint
            _logger.debug('appending global variables to context')
            g.stepper = self.stepper
            g.rig = self.rig
            g.restart_callable = self.restart

        self.connexion_app.register_api(api)

    @property
    def started(self):
        return not self._stop_event.is_set()

    def start(self):
        gevent.signal(signal.SIGTERM, self.stop)
        gevent.signal(signal.SIGINT, self.stop)
        gevent.signal(signal.SIGQUIT, self.stop)
        if self.daemon:
            gevent.signal(signal.SIGHUP, self.restart)

        while True:
            self._exit_status = 0
            self._loop_status = None

            with self.connexion_app.app.app_context():
                # load active configuration profile from database
                config_profile = load_config_profile()

            gevent.spawn(self._start, config_profile).join()

            if self._exit_status == 0 and (self._loop_status == 'restart' or self.daemon):
                continue

            break

        return self._exit_status

    def stop(self):
        _logger.info('stop signal received ...')
        self._loop_status = 'stop'
        self._stop_event.set()

    def restart(self):
        _logger.info('restart signal received ...')
        self._loop_status = 'restart'
        self._stop_event.set()

    def _start(self, config_profile):
        _logger.info('starting application...')

        self._stop_event.clear()

        self.stepper = None
        self.rig = None
        self.webapp = None
        self.menurunner = None

        try:
            # create controllers
            if config_profile.stepper.enabled:
                self.stepper = create_stepper(config_profile)

            if config_profile.rig.enabled:
                self.rig = create_rig(config_profile)

            if config_profile.webapp.enabled:
                self.webapp = create_webapp(config_profile, self.connexion_app)

            if config_profile.ui.enabled:
                self.menurunner = create_menurunner(create_virtualui(config_profile), self.rig, self.stepper)

            if not self.menurunner and not self.webapp:
                raise RuntimeError('Nothing to do - enable UI or WebApp and try again')

            if self.stepper:
                self.stepper.start()

            if self.rig:
                self.rig.start()

            if self.webapp:
                self.webapp.start()

            if self.menurunner:
                self.menurunner.start()

            # keep greenlet running
            self._stop_event.wait()
        except Exception as ex:
            _logger.exception('error during application execution...')
            self._exit_status = 1
        finally:
            self._stop()

    def _stop(self):
        _logger.info('stopping application...')

        try:
            if self.menurunner:
                self.menurunner.stop()

            if self.webapp:
                self.webapp.stop()

            if self.rig:
                self.rig.stop()

            if self.stepper:
                self.stepper.stop()
        except Exception as ex:
            _logger.exception('error stopping application...')
            self._exit_status = 2
