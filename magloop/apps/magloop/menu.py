########################################################################
# Filename    : menu.py
# Description : menu for mag loop controller application
########################################################################

import logging
import os
import signal
import time
from functools import wraps
from shutil import which
from subprocess import Popen

from libs.sysinfo import get_cpu_temp
from libs.sysinfo import get_cpu_utilisation
from libs.sysinfo import get_datetime_now
from libs.sysinfo import get_hostname
from libs.sysinfo import get_interface_ip
from libs.sysinfo import get_mem_free
from libs.ui.menu import MenuItemConfirmation
from libs.ui.menu import MenuItemScrollable

_logger = logging.getLogger(__name__)

_STEPPER_STEPS = [1, 10, 50, 100, 200, 400, 800, 1600]
_RIG_FREQ_STEPS = [10, 100, 1000, 10000, 100000, 1000000]


def log_exception(func):
    @wraps(func)
    def func_wrapper(self):
        ret = self
        try:
            ret = func(self)
        except Exception:
            _logger.exception('menu error')
        return ret

    return func_wrapper


class MenuSystemInfo(MenuItemScrollable):
    """Displays Systerm info"""
    _t = time.perf_counter()

    @log_exception
    def action(self):
        t = time.perf_counter()
        if t > self._t + 0.5:
            self._t = t
            width = self.cols - 9
            self.text = 'CPU Temp:{:>{width}}\n' \
                        'CPU Used:{:>{width}}\n' \
                        'Mem Free:{:>{width}}'.format(get_cpu_temp(), get_cpu_utilisation(), get_mem_free(),
                                                      width=width)
        return self


class MenuDateTime(MenuItemScrollable):
    """Displays Date/Time when select is pressed"""

    @log_exception
    def action(self):
        d = get_datetime_now()
        width = self.cols - 5
        self.text = 'Date:{:>{width}}\n' \
                    'Time:{:>{width}}' \
            .format(d['date'], d['time'], width=width)
        return self


class MenuLanIp(MenuItemScrollable):
    """Displays lan IP address when select is pressed"""

    @log_exception
    def action(self):
        ips = get_interface_ip()
        self.text = '{}\n' \
                    'v4:{}\n' \
                    'v6:{}' \
            .format(get_hostname(), ips['ipv4'], ips['ipv6'])
        return self


class MenuStepperInfo(MenuItemScrollable):
    """Displays stepper motor info when select is pressed"""

    def __init__(self, *args, stepper=None, **kwargs):
        super().__init__(*args, **kwargs)

        if stepper is None:
            raise TypeError('stepper must not be None')

        self._stepper = stepper

    @log_exception
    def action(self):
        width = self.cols - 13
        text = ''
        if hasattr(self._stepper, 'position'):
            text += 'Position    :{:>{width}}\n'.format(self._stepper.position, width=width)
        if hasattr(self._stepper, 'dir_gpio'):
            text += 'Directn GPIO:{:>{width}}\n'.format(self._stepper.dir_gpio, width=width)
        if hasattr(self._stepper, 'pul_gpio'):
            text += 'Pulse GPIO  :{:>{width}}\n'.format(self._stepper.pul_gpio, width=width)
        if hasattr(self._stepper, 'ena_gpio'):
            text += 'Enable GPIO :{:>{width}}\n'.format(self._stepper.ena_gpio, width=width)
        if hasattr(self._stepper, 'micro_step'):
            text += 'Micro Step  :{:>{width}}\n'.format(self._stepper.micro_step, width=width)
        if hasattr(self._stepper, 'ena_offline'):
            text += 'Enable Offln:{:>{width}}\n'.format(self._stepper.ena_offline, width=width)
        if hasattr(self._stepper, 'enable_delay_ms'):
            text += 'Enable Delay:{:>{width}}\n'.format(self._stepper.enable_delay_ms, width=width)

        self.text = text[:-1]
        return self


class MenuStepperMove(MenuItemScrollable):
    """Displays stepper motor info when select is pressed"""

    def __init__(self, *args, stepper=None, **kwargs):
        super().__init__(*args, **kwargs)

        if stepper is None:
            raise TypeError('stepper must not be None')

        self._stepper = stepper
        self._step = _STEPPER_STEPS[0]

    @log_exception
    def action(self):
        if self.rows < 3 or self.cols < 20:
            self.text = '{}P{:>6}{} {}{:>4}{}' \
                .format(self.nav_left, self._stepper.position, self.nav_right, self.nav_up, self._step, self.nav_down)
        else:
            width = self.cols - 8
            self.text = '{} Pos  {}{:>{width}}\n' \
                        '{} Step {}{:>{width}}' \
                .format(self.nav_left, self.nav_right, self._stepper.position, self.nav_up, self.nav_down, self._step,
                        width=width)
        return self

    @log_exception
    def left_press(self):
        if not self._stepper.busy:
            self._stepper.position -= self._step
        return self

    @log_exception
    def right_press(self):
        if not self._stepper.busy:
            self._stepper.position += self._step
        return self

    @log_exception
    def up_press(self):
        idx = _STEPPER_STEPS.index(self._step)
        if idx < len(_STEPPER_STEPS) - 1:
            self._step = _STEPPER_STEPS[idx + 1]
        return self

    @log_exception
    def down_press(self):
        idx = _STEPPER_STEPS.index(self._step)
        if idx > 0:
            self._step = _STEPPER_STEPS[idx - 1]
        return self

    @log_exception
    def select_press(self):
        self._stepper.cancelled = True
        return super().select_press()


class MenuStepperHome(MenuItemConfirmation):
    def __init__(self, *args, stepper=None, **kwargs):
        super().__init__(*args, **kwargs)

        if stepper is None:
            raise TypeError('stepper must not be None')

        self._stepper = stepper

    @log_exception
    def action(self):
        if self.rows < 3 or self.cols < 20:
            self._append_question_mark = False
            self.confirmation_text = None
        else:
            width = self.cols - 11
            self._append_question_mark = True
            self.confirmation_text = '[Position:{:>{width}}]'.format(self._stepper.position, width=width)
        super().action()
        return self

    def confirmed(self):
        self._stepper.position = 0
        return self.parent


class MenuRigBase(MenuItemScrollable):
    def __init__(self, *args, rig, restart_error_count=5, **kwargs):
        super().__init__(*args, **kwargs)

        if rig is None:
            raise TypeError('rig must not be None')

        self._rig = rig
        self._restart_error_count = restart_error_count
        self._error_count = 0

    def reset(self):
        self._error_count = 0

    def inc_error_count(self):
        self._error_count += 1

    def has_error(self):
        return self._error_count > 0

    def restart_required(self):
        # absorb a small number of errors to allow for intermittent issues
        if self._error_count >= self._restart_error_count:
            return True
        else:
            return False

    def get_menu_rig_restart(self):
        menu_rig_restart = MenuRigRestart(rig=self._rig, title='Rig Error',
                                          parent=self.parent,
                                          cb=self.reset,
                                          cols=self.cols, rows=self.rows,
                                          confirmation_yes='Restart',
                                          confirmation_no='Exit',
                                          confirmation_default=True,
                                          append_question_mark=False)
        menu_rig_restart.action()
        return menu_rig_restart


class MenuRigRestart(MenuItemConfirmation):
    def __init__(self, *args, rig=None, cb=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._rig = rig
        self._cb = cb

    def confirmed(self):
        self._rig.start()
        if self._cb:
            self._cb()
        return self.parent


class MenuRigInfo(MenuRigBase):
    """Displays rig info when select is pressed"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)

    @log_exception
    def action(self):
        freq_mhz = '?'
        rf_power = '?'
        vfo = '?'
        mode = '?'

        try:
            if self._rig.can_get_freq:
                freq_mhz = '{:.6f}'.format(self._rig.freq_mhz)
            if self._rig.can_get_rf_power:
                rf_power = self._rig.rf_power
            if self._rig.can_get_vfo:
                vfo = self._rig.vfo
            if self._rig.can_get_mode:
                mode = self._rig.mode
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            width = self.cols - 5
            self.text = 'Freq:{:>{width}}\n' \
                        'PWR :{:>{width}}\n' \
                        'VFO :{:>{width}}\n' \
                        'Mode:{:>{width}}' \
                .format(freq_mhz, rf_power, vfo, mode, width=width)

        return self


class MenuRigFrequency(MenuRigBase):
    """Displays rig frequency when select is pressed and adjust rig frequency with left/right"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)
        self._freq = 0
        self._step = _RIG_FREQ_STEPS[0]

    @log_exception
    def action(self):
        freq_mhz = '?'

        try:
            self._freq = self._rig.freq
            freq_mhz = self._rig.convert_frequency(self._freq, 'Hz', 'MHz')
            freq_mhz = '{:.6f}'.format(freq_mhz)
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            if self.rows < 3 or self.cols < 20:
                exponent = _RIG_FREQ_STEPS.index(self._step) + 1
                self.text = '{}{}{} {}e{}{}' \
                    .format(self.nav_left, freq_mhz, self.nav_right, self.nav_up, exponent, self.nav_down)
            else:
                width = self.cols - 8
                self.text = '{} Freq {}{:>{width}}\n' \
                            '{} Step {}{:>{width}}' \
                    .format(self.nav_left, self.nav_right, freq_mhz, self.nav_up, self.nav_down, self._step,
                            width=width)

        return self

    @log_exception
    def left_press(self):
        self._rig.freq = self._freq - self._step
        return self

    @log_exception
    def right_press(self):
        self._rig.freq = self._freq + self._step
        return self

    @log_exception
    def up_press(self):
        idx = _RIG_FREQ_STEPS.index(self._step)
        if idx < len(_RIG_FREQ_STEPS) - 1:
            self._step = _RIG_FREQ_STEPS[idx + 1]
        return self

    @log_exception
    def down_press(self):
        idx = _RIG_FREQ_STEPS.index(self._step)
        if idx > 0:
            self._step = _RIG_FREQ_STEPS[idx - 1]
        return self


class MenuRigPower(MenuRigBase):
    """Displays rig power when select is pressed and adjust rig power with left/right"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)
        self._power = 0

    @log_exception
    def action(self):
        try:
            self._power = self._rig.rf_power
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            width = self.cols - 9
            self.text = '{} Power {}{:>{width}}' \
                .format(self.nav_left, self.nav_right, self._power, width=width)
        return self

    @log_exception
    def left_press(self):
        if self._power > 0:
            self._rig.rf_power = self._power - 5
        return self

    @log_exception
    def right_press(self):
        if self._power < 100:
            self._rig.rf_power = self._power + 5
        return self


class MenuRigVfo(MenuRigBase):
    """Displays VFO when select is pressed and adjust rig VFO with left/right"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)
        self._vfo = ''

    @log_exception
    def action(self):
        try:
            self._vfo = self._rig.vfo
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            width = self.cols - 7
            self.text = '{} VFO {}{:>{width}}' \
                .format(self.nav_left, self.nav_right, self._vfo, width=width)
        return self

    @log_exception
    def left_press(self):
        if self._vfo in self._rig.vfos_available:  # check for case when rig is reporting a current value that isn't in available list
            idx = self._rig.vfos_available.index(self._vfo)
            if idx > 0:
                self._rig.vfo = self._rig.vfos_available[idx - 1]
        else:
            self._rig.vfo = self._rig.vfos_available[0]
        return self

    @log_exception
    def right_press(self):
        if self._vfo in self._rig.vfos_available:  # check for case when rig is reporting a current value that isn't in available list
            idx = self._rig.vfos_available.index(self._vfo)
            if idx < len(self._rig.vfos_available) - 1:
                self._rig.vfo = self._rig.vfos_available[idx + 1]
        else:
            self._rig.vfo = self._rig.vfos_available[0]
        return self


class MenuRigMode(MenuRigBase):
    """Displays Mode when select is pressed and adjust rig Mode with left/right"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)
        self._mode = ''

    @log_exception
    def action(self):
        try:
            self._mode = self._rig.mode
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            width = self.cols - 8
            self.text = '{} MODE {}{:>{width}}' \
                .format(self.nav_left, self.nav_right, self._mode, width=width)
        return self

    @log_exception
    def left_press(self):
        if self._mode in self._rig.modes_available:
            idx = self._rig.modes_available.index(self._mode)
            if idx > 0:
                self._rig.mode = self._rig.modes_available[idx - 1]
        else:
            self._rig.mode = self._rig.modes_available[0]
        return self

    @log_exception
    def right_press(self):
        if self._mode in self._rig.modes_available:
            idx = self._rig.modes_available.index(self._mode)
            if idx < len(self._rig.modes_available) - 1:
                self._rig.mode = self._rig.modes_available[idx + 1]
        else:
            self._rig.mode = self._rig.modes_available[0]
        return self


class MenuRigPTT(MenuRigBase):
    """Displays PTT when select is pressed and adjust rig PTT with left/right"""

    def __init__(self, *args, rig=None, **kwargs):
        super().__init__(*args, rig=rig, **kwargs)
        self._ptt = False

    @log_exception
    def action(self):
        try:
            self._ptt = self._rig.ptt
            self.reset()
        except:
            _logger.exception('rig error')
            self.inc_error_count()

        if self.restart_required():
            self.reset()
            return self.get_menu_rig_restart()
        elif self.has_error():
            self.text = 'Connecting...'
            self._rig.open()  # attempt automatic reopen
        else:
            width = self.cols - 7
            self.text = '{} PTT {}{:>{width}}' \
                .format(self.nav_left, self.nav_right, 'On' if self._ptt else 'Off', width=width)
        return self

    @log_exception
    def left_press(self):
        if self._ptt == True:
            self._rig.ptt = False
        return self

    @log_exception
    def right_press(self):
        if self._ptt == False:
            self._rig.ptt = True
        return self


class MenuReboot(MenuItemConfirmation):
    def confirmed(self):
        cmd = ['reboot']
        Popen(cmd)
        os.kill(os.getpid(), signal.SIGINT)
        return self

    @property
    def available(self):
        return which('reboot') != None


class MenuShutdown(MenuItemConfirmation):
    def confirmed(self):
        cmd = ['shutdown', '-h', 'now']
        Popen(cmd)
        os.kill(os.getpid(), signal.SIGINT)
        return self

    @property
    def available(self):
        return which('shutdown') != None
