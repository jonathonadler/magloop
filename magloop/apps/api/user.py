########################################################################
# Filename    : user.py
# Description : User openapi controller
########################################################################

from connexion import NoContent, problem, ProblemException

from models import User
from schemas import user_schema, user_schema_create, users_schema
from . import dict2obj
from .auth import is_admin


def _get_user_or_404(id):
    user = User.get_one_user(id)
    if user:
        return user
    else:
        raise ProblemException(404, 'User not found', 'No user was found with the value specified.')


def _get_user_by_username_or_404(username):
    user = User.get_user_by_username(username)
    if user:
        return user
    else:
        raise ProblemException(404, 'User not found', 'No user was found with the value specified.')


@is_admin
def get_users(user, token_info):
    return users_schema.dump(User.get_all_users()).data


@is_admin
def post_user(user, token_info, body):
    if User.get_user_by_username(body.get('username')):
        return problem(409, 'User conflict', 'User already exists with specified username.')

    u = user_schema_create.load(body).data
    u.save()

    return (user_schema.dump(u).data, 201)


@is_admin
def delete_user(user, token_info, id, type):
    if type == 'username':
        u = _get_user_by_username_or_404(id)
    else:
        u = _get_user_or_404(id)

    u.delete()

    return (NoContent, 204)


@is_admin
def put_user(user, token_info, id, type, body):
    if type == 'username':
        u = _get_user_by_username_or_404(id)
    else:
        u = _get_user_or_404(id)

    if User.get_user_by_username(body.get('username')).id != u.id:
        return problem(409, 'User conflict', 'User already exists with specified username.')

    for k, v in body.items():
        setattr(u, k, v)

    u.update()

    return user_schema.dump(u).data


def get_user(user, token_info, id, type):
    if type == 'username':
        u = _get_user_by_username_or_404(id)
    else:
        u = _get_user_or_404(id)

    # admins can get any record, non-admins can get their own record
    is_admin = token_info.get('is_admin')
    is_authenticated_user = u.username == user
    if not (is_admin or is_authenticated_user):
        return problem(403, 'You don\'t have the permission to access the requested resource.',
                       'It is either read-protected or not readable by the server.')

    return user_schema.dump(u).data


def patch_user(user, token_info, id, type, body):
    if type == 'username':
        u = _get_user_by_username_or_404(id)
    else:
        u = _get_user_or_404(id)

    # admins can modify any record, non-admins can modify their own record
    is_admin = token_info.get('is_admin')
    is_authenticated_user = u.username == user
    if not (is_admin or is_authenticated_user):
        return problem(403, 'You don\'t have the permission to access the requested resource.',
                       'It is either read-protected or not readable by the server.')

    if body.get('is_admin') and not is_admin:
        return problem(403, 'You don\'t have the permission to set is_admin property.',
                       'Only administrator users may set this property.')

    if 'username' in body and User.get_user_by_username(body.get('username')).id != u.id:
        return problem(409, 'User conflict', 'User already exists with specified username.')

    dict2obj(body, u)

    u.update()

    return user_schema.dump(u).data
