########################################################################
# Filename    : radio.py
# Description : Radio openapi controller
########################################################################

import logging

from connexion import problem
from flask import g

_logger = logging.getLogger(__name__)


def get_radio(user, token_info):
    rig = g.rig

    if rig:
        try:
            result = {
                'model_name': rig.hamlib_model_name,
                'mfg_name': rig.hamlib_mfg_name,
                'version': rig.hamlib_version,
                'status': rig.hamlib_status,
                'modes_available': rig.modes_available,
                'vfos_available': rig.vfos_available,
                'can_set_freq': rig.can_set_freq,
                'can_get_freq': rig.can_get_freq,
                'can_set_mode': rig.can_set_mode,
                'can_get_mode': rig.can_get_mode,
                'can_set_rf_power': rig.can_set_rf_power,
                'can_get_rf_power': rig.can_get_rf_power,
                'can_set_vfo': rig.can_set_vfo,
                'can_get_vfo': rig.can_get_vfo,
                'can_set_ptt': rig.can_set_ptt,
                'can_get_ptt': rig.can_get_ptt,
            }

            if rig.can_get_freq:
                result['frequency'] = rig.freq
            if rig.can_get_mode:
                result['mode'] = rig.mode
            if rig.can_get_rf_power:
                result['rf_power'] = rig.rf_power
            if rig.can_get_vfo:
                result['vfo'] = rig.vfo
            if rig.can_get_ptt:
                result['ptt'] = rig.ptt

            return result
        except Exception as ex:
            return problem(503, 'Radio not available', ex.__str__())
    else:
        return problem(404, 'Radio not found',
                       'No radio was found. Application must be started with --rig command line switch.')


def patch_radio(user, token_info, body):
    rig = g.rig

    if rig.can_set_freq and 'frequency' in body:
        rig.freq = body.get('frequency')
    if rig.can_set_mode and 'mode' in body:
        rig.mode = body.get('mode')
    if rig.can_set_rf_power and 'rf_power' in body:
        rig.rf_power = body.get('rf_power')
    if rig.can_set_vfo and 'vfo' in body:
        rig.vfo = body.get('vfo')
    if rig.can_set_ptt and 'ptt' in body:
        rig.ptt = body.get('ptt')

    return get_radio(user, token_info)
