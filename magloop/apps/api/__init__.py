def dict2obj(dict_, obj):
    """
    recursively map a dictionary to an object
    :param dict_: dictionary (source)
    :param obj: object (target)
    """
    for key, value in dict_.items():
        if isinstance(value, (dict)):
            dict2obj(value, getattr(obj, key))
        else:
            setattr(obj, key, value)
