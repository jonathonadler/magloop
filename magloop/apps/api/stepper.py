########################################################################
# Filename    : stepper.py
# Description : Stepper motor openapi controller
########################################################################

import logging

from connexion import problem
from flask import g

_logger = logging.getLogger(__name__)


def get_stepper(user, token_info):
    stepper = g.stepper

    if stepper:
        try:
            result = {
                'position': stepper.position,
                'micro_step': stepper.micro_step,
            }

            if hasattr(stepper, 'enable'):
                result['offline'] = stepper.enable

            return result
        except Exception as ex:
            return problem(503, 'Stepper not available', ex.__str__())
    else:
        return problem(404, 'Stepper not found',
                       'No stepper was found. Application must be started with --stepper command line switch.')


def patch_stepper(user, token_info, body):
    stepper = g.stepper

    if 'position' in body:
        stepper.position = body.get('position')

    return get_stepper(user, token_info)
