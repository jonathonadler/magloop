########################################################################
# Filename    : config_profile.py
# Description : Configuration profile openapi controller
########################################################################

from connexion import problem, ProblemException
from flask import g

from models import ConfigurationProfile
from schemas import config_profiles_schema, config_profile_schema
from . import dict2obj
from .auth import is_admin


def _get_config_profile_or_404(id):
    if id == 'active':
        config_profile = ConfigurationProfile.get_active_profile()
    else:
        config_profile = ConfigurationProfile.get_one_profile(id)

    if config_profile:
        return config_profile
    else:
        raise ProblemException(404, 'Configuration profile not found',
                               'No configuration profile was found with the value specified.')


def get_config_profiles(user, token_info):
    return config_profiles_schema.dump(ConfigurationProfile.get_all_profiles()).data


def get_config_profile(user, token_info, id):
    config_profile = _get_config_profile_or_404(id)

    try:
        return config_profile_schema.dump(config_profile).data
    except Exception as ex:
        return problem(503, 'Configuration profile not available', ex.__str__())


@is_admin
def patch_config_profile(user, token_info, id, body):
    if 'is_active' in body and not body.get('is_active'):
        raise ProblemException(400, 'The is_active property can only be set true.',
                               'One configuration profile must be active at all times. Set is_active property true to make a configuration profile active.')

    config_profile = _get_config_profile_or_404(id)

    dict2obj(body, config_profile)

    config_profile.update()

    ret = config_profile_schema.dump(config_profile).data

    if config_profile.is_active:
        g.restart_callable()

    return ret
