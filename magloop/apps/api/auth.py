########################################################################
# Filename    : auth.py
# Description : Authorisation openapi controller
########################################################################

import logging
import time
from functools import wraps

import jwt
from cachelib import SimpleCache
from connexion import problem, FlaskApi
from sqlalchemy import event
from werkzeug.exceptions import Unauthorized

import config
from models import User

_logger = logging.getLogger(__name__)
_user_cache = SimpleCache()


def _current_timestamp() -> int:
    return int(time.time())


def _get_user_by_username(username):
    user_key = username.lower()
    user = _user_cache.get(user_key)

    if user is None:
        user = User.get_user_by_username(username)
        _user_cache.set(user_key, user, timeout=config.USER_CACHE_TIMEOUT_SECONDS)
        _logger.debug('set cache key={}'.format(user_key))

    return user


def basic_auth(username, password, required_scopes=None):
    user = _get_user_by_username(username)

    if user and user.check_password(password):
        return {'sub': user.username, 'scope': user.username, 'is_admin': user.is_admin}

    return None


def apikey_auth(apikey, required_scopes=None):
    return jwt_auth(apikey)


def jwt_auth(token):
    try:
        return jwt.decode(token, config.JWT_SECRET, algorithms=[config.JWT_ALGORITHM])
    except jwt.DecodeError:
        raise Unauthorized()


def get_access_token(user, token_info):
    timestamp = _current_timestamp()
    issued_at = int(timestamp)
    expiration_time = int(timestamp + config.JWT_LIFETIME_SECONDS)

    payload = {
        "iss": config.JWT_ISSUER,
        "iat": issued_at,
        "exp": expiration_time,
        "sub": str(user),
        "scope": str(user),
        "is_admin": True if token_info and token_info['is_admin'] else False
    }

    access_token = jwt.encode(payload, config.JWT_SECRET, algorithm=config.JWT_ALGORITHM).decode('utf-8')

    response = FlaskApi.get_response({'access_token': access_token}, 'application/json')
    response.set_cookie('access_token', value=access_token, expires=expiration_time, httponly=True, samesite='Strict')
    return response


def is_admin(f):
    @wraps(f)
    def is_admin_decorator(*args, **kwargs):
        try:
            is_admin = kwargs['token_info']['is_admin']
        except (KeyError, AttributeError):
            is_admin = False

        _logger.debug('is_admin={}'.format(is_admin))

        if not is_admin:
            return problem(403,
                           'You don\'t have the permission to access the requested resource.',
                           'It is either read-protected or not readable by the server.')

        return f(*args, **kwargs)

    return is_admin_decorator


@event.listens_for(User, 'after_update')
def receive_after_update(mapper, connection, target):
    _user_cache.delete(target.username.lower())


@event.listens_for(User, 'after_delete')
def receive_after_delete(mapper, connection, target):
    _user_cache.delete(target.username.lower())
