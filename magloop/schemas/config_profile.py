########################################################################
# Filename    : config_profile.py
# Description : model schema
########################################################################

from models import ConfigurationProfile
from models import RigConfigurationProfile
from models import StepperConfigurationProfile
from models import UiConfigurationProfile
from models import WebappConfigurationProfile
from . import ma


class UiConfigurationProfileSchema(ma.ModelSchema):
    class Meta:
        model = UiConfigurationProfile


class StepperConfigurationProfileSchema(ma.ModelSchema):
    class Meta:
        model = StepperConfigurationProfile


class RigConfigurationProfileSchema(ma.ModelSchema):
    class Meta:
        model = RigConfigurationProfile


class WebappConfigurationProfileSchema(ma.ModelSchema):
    class Meta:
        model = WebappConfigurationProfile


class ConfigurationProfileSchema(ma.ModelSchema):
    class Meta:
        model = ConfigurationProfile

    ui = ma.Nested(UiConfigurationProfileSchema, exclude=('id', 'profile'))
    stepper = ma.Nested(StepperConfigurationProfileSchema, exclude=('id', 'profile'))
    rig = ma.Nested(RigConfigurationProfileSchema, exclude=('id', 'profile'))
    webapp = ma.Nested(WebappConfigurationProfileSchema, exclude=('id', 'profile'))


config_profile_schema = ConfigurationProfileSchema(strict=True)
config_profiles_schema = ConfigurationProfileSchema(strict=True, many=True)
