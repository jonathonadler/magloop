########################################################################
# Filename    : user.py
# Description : model schema
########################################################################

from models import User
from . import ma


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        exclude = ['password_hash']


class UserSchemaCreate(ma.ModelSchema):
    class Meta:
        model = User
        exclude = ['password_hash', 'created_at', 'modified_at']

    password = ma.Str()


user_schema = UserSchema(strict=True)
user_schema_create = UserSchemaCreate(strict=True)
users_schema = UserSchema(strict=True, many=True)
