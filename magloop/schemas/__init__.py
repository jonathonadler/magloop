from flask_marshmallow import Marshmallow

ma = Marshmallow()

from .config_profile import ConfigurationProfileSchema
from .config_profile import config_profile_schema
from .config_profile import config_profiles_schema
from .user import UserSchema
from .user import user_schema
from .user import user_schema_create
from .user import users_schema
