########################################################################
# Filename    : enum.py
# Description : Enumerations
########################################################################

import enum

_GPIOS = [('GPIO_{}'.format(i), i) for i in range(32)]
_I2C_Addresses = [('PORT_0x{:02x}'.format(i), '0x{:02x}'.format(i)) for i in range(int('0x7f', 16) + 1)]

GPIO = enum.Enum('GPIO', _GPIOS)
I2C_Address = enum.Enum('I2C_Address', _I2C_Addresses)


class I2C_Expander(enum.Enum):
    PCF8574 = 'PCF8574'
    MCP23008 = 'MCP23008'
    MCP23017 = 'MCP23017'


class LcdCharmap(enum.Enum):
    A00 = 'A00'
    A02 = 'A02'


class StepperDriver(enum.Enum):
    GENERIC = 'GENERIC'
    MICROSTEP = 'MICROSTEP'


class StepperMicrostep(enum.Enum):
    MICROSTEP_1 = 1
    MICROSTEP_2 = 2
    MICROSTEP_4 = 4
    MICROSTEP_8 = 8
    MICROSTEP_16 = 16
    MICROSTEP_32 = 32


class StepperEnableOffline(enum.Enum):
    LOW = 0
    HIGH = 1


class RigPttType(enum.Enum):
    RIG = 'RIG'
    DTR = 'DTR'
    RTS = 'RTS'
    PARALLEL = 'Parallel'
    CM108 = 'CM108'
    GPIO = 'GPIO'
    GPION = 'GPION'
    NONE = 'None'


class RigSerialSpeed(enum.Enum):
    BAUD_75 = 75
    BAUD_110 = 110
    BAUD_300 = 300
    BAUD_1200 = 1200
    BAUD_2400 = 2400
    BAUD_4800 = 4800
    BAUD_9600 = 9600
    BAUD_19200 = 19200
    BAUD_38400 = 38400
    BAUD_57600 = 57600
    BAUD_115200 = 115200


class RigSerialDataBits(enum.Enum):
    DATA_BIT_5 = 5
    DATA_BIT_6 = 6
    DATA_BIT_7 = 7
    DATA_BIT_8 = 8


class RigSerialStopBits(enum.Enum):
    STOP_BIT_0 = 0
    STOP_BIT_1 = 1


class RigSerialParity(enum.Enum):
    NONE = 'None'
    ODD = 'Odd'
    EVEN = 'Even'
    MARK = 'Mark'
    SPACE = 'Space'


class RigSerialHandshake(enum.Enum):
    NONE = 'None'
    XONXOFF = 'XONXOFF'
    HARDWARE = 'Hardware'


class RigSerialRtsState(enum.Enum):
    UNSET = 'Unset'
    ON = 'ON'
    OFF = 'OFF'


class RigSerialDtrState(enum.Enum):
    UNSET = 'Unset'
    ON = 'ON'
    OFF = 'OFF'
