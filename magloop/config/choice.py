########################################################################
# Filename    : choice.py
# Description : Choices
########################################################################

import config.enum as enums

GPIO = [e.value for e in enums.GPIO]
I2C_Expander = [e.value for e in enums.I2C_Expander]
I2C_Address = [e.value for e in enums.I2C_Address]
LcdCharmap = [e.value for e in enums.LcdCharmap]
StepperDriver = [e.value for e in enums.StepperDriver]
StepperMicrostep = [e.value for e in enums.StepperMicrostep]
StepperEnableOffline = [e.value for e in enums.StepperEnableOffline]
RigPttType = [e.value for e in enums.RigPttType]
RigSerialSpeed = [e.value for e in enums.RigSerialSpeed]
RigSerialDataBits = [e.value for e in enums.RigSerialDataBits]
RigSerialStopBits = [e.value for e in enums.RigSerialStopBits]
RigSerialParity = [e.value for e in enums.RigSerialParity]
RigSerialHandshake = [e.value for e in enums.RigSerialHandshake]
RigSerialRtsState = [e.value for e in enums.RigSerialRtsState]
RigSerialDtrState = [e.value for e in enums.RigSerialDtrState]
