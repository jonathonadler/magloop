########################################################################
# Description : Configuration and defaults for magnetic loop application
########################################################################

from pathlib import Path

import config.enum as enums

# logging
LOG_CFG = '{}/logging.yaml'.format(Path(__file__).resolve().parent)

# sqlalchemy
SQLALCHEMY_TRACK_MODIFICATIONS = False

# flask-sqlalchemy
DATABASE_URI = 'sqlite:///{}/magloop.db'.format(Path.home())

USER_CACHE_TIMEOUT_SECONDS = 300

# ui (lcd/joystick)
UI_LCD_I2C_EXPANDER = enums.I2C_Expander.PCF8574.value
UI_LCD_ADDRESS = enums.I2C_Address.PORT_0x27.value
UI_LCD_COLS = 20
UI_LCD_ROWS = 4
UI_LCD_CHARMAP = enums.LcdCharmap.A00.value
UI_LCD_BACKLIGHT_TIMEOUT = 120
UI_JOYSTICK_ADDRESS = enums.I2C_Address.PORT_0x48.value
UI_JOYSTICK_Z_GPIO = enums.GPIO.GPIO_18.value

# stepper
STEPPER_DRIVER = enums.StepperDriver.GENERIC.value
STEPPER_ENA_OFFLINE = enums.StepperEnableOffline.LOW.value  # LOW (0) by default (TB6600). Set to HIGH (1) for DRV8825/A4988 etc.
STEPPER_ENA_GPIO = enums.GPIO.GPIO_19.value
STEPPER_ENA_DELAY_MS = -1
STEPPER_DIR_GPIO = enums.GPIO.GPIO_25.value
STEPPER_PUL_GPIO = enums.GPIO.GPIO_24.value
STEPPER_MICRO_STEP = enums.StepperMicrostep.MICROSTEP_1.value
STEPPER_MICRO_STEP_SELECT1_GPIO = enums.GPIO.GPIO_16.value
STEPPER_MICRO_STEP_SELECT2_GPIO = enums.GPIO.GPIO_20.value
STEPPER_MICRO_STEP_SELECT3_GPIO = enums.GPIO.GPIO_21.value

# rig (hamlib)
RIG_MODEL_ID = 1
RIG_TIMEOUT_MS = 500
RIG_RETRY = 0

# webapp (http)
WEBAPP_ADDRESS = '0.0.0.0'
WEBAPP_PORT = 8080

# javascript web token
JWT_ISSUER = 'com.magloop'
JWT_SECRET = 'JWT_SECRET'  # updated at runtime (from settings table)
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
