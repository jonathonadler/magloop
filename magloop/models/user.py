########################################################################
# Filename    : user.py
# Description : database model
########################################################################

import datetime

import sqlalchemy.types as types
from flask_bcrypt import generate_password_hash, check_password_hash

from . import db


class LowerCaseString(types.TypeDecorator):
    '''Converts strings to lower case on the way in.'''

    impl = types.String

    def process_bind_param(self, value, dialect):
        return value.lower()


class User(db.Model):
    """
    User Model
    """

    __tablename__ = 'user'

    id = db.Column(db.Integer, db.Sequence('user_id_seq'), primary_key=True)
    username = db.Column(LowerCaseString(128), unique=True, nullable=False, index=True)
    password_hash = db.Column(db.String(128), nullable=False)
    full_name = db.Column(db.String(128), nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    created_at = db.Column(db.DateTime, nullable=False)
    modified_at = db.Column(db.DateTime, nullable=False)

    # class constructor
    def __init__(self, username=None, password=None, full_name=None, is_admin=None):
        """
        Class constructor
        """
        self.username = username
        self.password = password
        self.full_name = full_name
        self.is_admin = is_admin
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, value):
        self.password_hash = generate_password_hash(value, 10).decode("utf-8")

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def get_user_by_username(value):
        return User.query.filter(User.username == value.lower()).first()

    @staticmethod
    def get_all_users():
        return User.query.all()

    @staticmethod
    def get_one_user(id):
        return User.query.get(id)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User: id {}>'.format(self.id)
