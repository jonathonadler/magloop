########################################################################
# Filename    : config_profile.py
# Description : database model
########################################################################

import datetime

from . import db

CONFIG_PROFILE_SECTIONS = ['ui', 'stepper', 'rig', 'webapp']


class ConfigurationProfile(db.Model):
    """
    Configuration Profile Model
    """

    __tablename__ = 'profile'

    id = db.Column(db.Integer, db.Sequence('profile_id_seq', start=1, minvalue=1, maxvalue=10), primary_key=True)
    is_active = db.Column(db.Boolean, default=False, nullable=False, index=True)

    ui = db.relationship('UiConfigurationProfile', uselist=False, back_populates='profile', lazy='joined')
    stepper = db.relationship('StepperConfigurationProfile', uselist=False, back_populates='profile', lazy='joined')
    rig = db.relationship('RigConfigurationProfile', uselist=False, back_populates='profile', lazy='joined')
    webapp = db.relationship('WebappConfigurationProfile', uselist=False, back_populates='profile', lazy='joined')

    created_at = db.Column(db.DateTime, nullable=False)
    modified_at = db.Column(db.DateTime, nullable=False)

    # class constructor
    def __init__(self):
        """
        Class constructor
        """
        self.ui = UiConfigurationProfile()
        self.stepper = StepperConfigurationProfile()
        self.rig = RigConfigurationProfile()
        self.webapp = WebappConfigurationProfile()
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def get_all_profiles():
        return ConfigurationProfile.query.all()

    @staticmethod
    def get_one_profile(id):
        return ConfigurationProfile.query.get(id)

    @staticmethod
    def get_active_profile():
        return ConfigurationProfile.query.filter(ConfigurationProfile.is_active == True).first()

    def __repr__(self):
        return '<Profile: id {}>'.format(self.id)


@db.event.listens_for(ConfigurationProfile, 'before_update')
def receive_before_update(mapper, connection, target):
    if target.is_active:
        # ensure only one configuration profile is active at any one time
        stmt = ConfigurationProfile.__table__.update(). \
            where(ConfigurationProfile.id != target.id). \
            values(is_active=False)
        db.engine.execute(stmt)


class UiConfigurationProfile(db.Model):
    """
    UI Configuration Profile Model
    """

    __tablename__ = 'profile_ui'

    id = db.Column(db.Integer, db.Sequence('profile_ui_id_seq'), primary_key=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('profile.id'))
    profile = db.relationship('ConfigurationProfile', back_populates='ui')

    enabled = db.Column(db.Boolean, default=False, nullable=False)
    lcd_i2c_expander = db.Column(db.String)
    lcd_address = db.Column(db.String)
    lcd_cols = db.Column(db.Integer)
    lcd_rows = db.Column(db.Integer)
    lcd_charmap = db.Column(db.String)
    lcd_backlight_timeout = db.Column(db.Integer)
    joy_address = db.Column(db.String)
    joy_z_gpio = db.Column(db.Integer)


class StepperConfigurationProfile(db.Model):
    """
    Stepper Configuration Profile Model
    """

    __tablename__ = 'profile_stepper'

    id = db.Column(db.Integer, db.Sequence('profile_stepper_id_seq'), primary_key=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('profile.id'))
    profile = db.relationship('ConfigurationProfile', back_populates='stepper')

    enabled = db.Column(db.Boolean, default=False, nullable=False)
    driver = db.Column(db.String)
    dir_gpio = db.Column(db.Integer)
    pul_gpio = db.Column(db.Integer)
    micro_step = db.Column(db.Integer)
    ena_offline = db.Column(db.Integer)
    ena_gpio = db.Column(db.Integer)
    ena_delay = db.Column(db.Integer)
    micro_step_select1_gpio = db.Column(db.Integer)
    micro_step_select2_gpio = db.Column(db.Integer)
    micro_step_select3_gpio = db.Column(db.Integer)


class RigConfigurationProfile(db.Model):
    """
    Rig Configuration Profile Model
    """

    __tablename__ = 'profile_rig'

    id = db.Column(db.Integer, db.Sequence('profile_rig_id_seq'), primary_key=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('profile.id'))
    profile = db.relationship('ConfigurationProfile', back_populates='rig')

    enabled = db.Column(db.Boolean, default=False, nullable=False)
    model_id = db.Column(db.Integer)
    pathname = db.Column(db.String)
    timeout = db.Column(db.Integer)
    retry = db.Column(db.Integer)
    ptt_type = db.Column(db.String)
    ptt_pathname = db.Column(db.String)
    ptt_bitnum = db.Column(db.Integer)
    serial_speed = db.Column(db.Integer)
    serial_data_bits = db.Column(db.Integer)
    serial_stop_bits = db.Column(db.Integer)
    serial_parity = db.Column(db.String)
    serial_handshake = db.Column(db.String)
    serial_rts_state = db.Column(db.String)
    serial_dtr_state = db.Column(db.String)


class WebappConfigurationProfile(db.Model):
    """
    Webapp Configuration Profile Model
    """

    __tablename__ = 'profile_webapp'

    id = db.Column(db.Integer, db.Sequence('profile_webapp_id_seq'), primary_key=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('profile.id'))
    profile = db.relationship('ConfigurationProfile', back_populates='webapp')

    enabled = db.Column(db.Boolean, default=False, nullable=False)
    address = db.Column(db.String)
    port = db.Column(db.Integer)
