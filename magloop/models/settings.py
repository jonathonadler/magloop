########################################################################
# Filename    : settings.py
# Description : global settings database model
########################################################################

import codecs
import os

from . import db
import datetime

class Settings(db.Model):
    """
    Settings Model
    """

    __tablename__ = 'settings'

    _id = db.Column(db.Integer, primary_key=True)

    jwt_secret = db.Column(db.String(64), nullable=False)

    created_at = db.Column(db.DateTime, nullable=False)
    modified_at = db.Column(db.DateTime, nullable=False)

    # class constructor
    def __init__(self):
        """
        Class constructor
        """
        self._id = 1
        self.jwt_secret = codecs.encode(os.urandom(32), 'hex').decode('utf-8')
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def get():
        return Settings.query.first()

    def __repr__(self):
        return '<Settings: id {}>'.format(self.id)
