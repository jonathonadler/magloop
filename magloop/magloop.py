########################################################################
# Filename    : magloop.py
# Description : Magnetic Loop Antenna Controller
########################################################################

import argparse
import logging
import os
import sys

from tabulate import tabulate

import config
import config.choice as choice

_logger = logging.getLogger(__name__)

if __name__ == "__main__":
    # override convert_arg_line_to_args. change from default (was one command per line) to all commands on a single line
    argparse.ArgumentParser.convert_arg_line_to_args = lambda self, arg_line: arg_line.split()

    parser = argparse.ArgumentParser(
        fromfile_prefix_chars='@',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        argument_default=argparse.SUPPRESS,
        description='Magnetic Loop Antenna Controller',
        epilog='''Stepper motors typically have a step size specification (e.g. 1.8° or 200 steps per revolution), which 
        applies to full steps. A microstepping driver such as the A4988 or DRV8825 allows higher resolutions by allowing 
        intermediate step locations, which are achieved by energizing the coils with intermediate current levels. For 
        instance, driving a motor in quarter-step mode will give the 200-step-per-revolution motor 800 microsteps per 
        revolution by using four different current levels. For further information, see: 
        https://www.pololu.com/product/1183 and https://www.pololu.com/product/2133'''
    )
    parser.prog = os.path.basename(sys.argv[0])

    parser.add_argument('@<filename>',
                        help='read command line arguments from file',
                        nargs='?')

    parser.add_argument('-d', '--daemon',
                        action='store_true',
                        help='restart after crash')

    command_group = parser.add_argument_group('commands')
    command_group.add_argument('--show-rigs',
                               action='store_true',
                               help='show available Hamlib rigs and exit')

    log_group = parser.add_argument_group('logging')
    log_group.add_argument('--log-cfg',
                           default=config.LOG_CFG,
                           help='log configuration file in YAML format')
    log_group.add_argument('--debug',
                           action='store_true',
                           help='enable debug mode')
    log_group.add_argument('--trace',
                           action='store_true',
                           help='enable trace mode')

    db_group = parser.add_argument_group('database')
    db_group.add_argument('--database-uri',
                          default=config.DATABASE_URI,
                          help='database connection string')

    config_profile_group = parser.add_argument_group('configuration profile')
    config_profile_group.add_argument('--config-profile-id',
                                      type=int,
                                      choices=range(1, 11),
                                      help='configuration profile id')

    lcd_group = parser.add_argument_group('ui')
    lcd_group.add_argument('--ui',
                           action='store_true',
                           help='enable the user interface')
    lcd_group.add_argument('--ui-lcd-i2c-expander',
                           choices=choice.I2C_Expander,
                           help='lcd I2C expander (port type)'
                           )
    lcd_group.add_argument('--ui-lcd-address',
                           choices=choice.I2C_Address,
                           help='lcd I2C address'
                           )
    lcd_group.add_argument('--ui-lcd-cols',
                           type=int,
                           help='lcd columns'
                           )
    lcd_group.add_argument('--ui-lcd-rows',
                           type=int,
                           help='lcd rows'
                           )
    lcd_group.add_argument('--ui-lcd-charmap',
                           choices=choice.LcdCharmap,
                           help='lcd charmap'
                           )
    lcd_group.add_argument('--ui-lcd-backlight-timeout',
                           type=int,
                           help='lcd backlight timeout (s) [-1=timeout]'
                           )
    lcd_group.add_argument('--ui-joy-address',
                           choices=choice.I2C_Address,
                           help='joystick I2C address'
                           )
    lcd_group.add_argument('--ui-joy-z-gpio',
                           choices=choice.GPIO,
                           type=int,
                           help='joystick z button GPIO'
                           )

    stepper_group = parser.add_argument_group('stepper')
    stepper_group.add_argument('--stepper',
                               action='store_true',
                               help='enable the stepper motor driver')
    stepper_group.add_argument('--stepper-driver',
                               choices=choice.StepperDriver,
                               help='stepper driver type. Select GENERIC if microstep setting is hardware controlled. '
                                    'Select MICROSTEP if microstep setting is contolled via GPIO (requires --stepper-micro-step-modeX)'
                               )
    stepper_group.add_argument('--stepper-dir-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver direction GPIO'
                               )
    stepper_group.add_argument('--stepper-pul-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver pulse GPIO'
                               )
    stepper_group.add_argument('--stepper-micro-step',
                               type=int,
                               choices=choice.StepperMicrostep,
                               help='stepper microstep setting (number of pulses per full step)'
                               )
    stepper_group.add_argument('--stepper-ena-offline',
                               type=int,
                               choices=choice.StepperEnableOffline,
                               help='stepper driver enable offline mode (0=Low, 1=High). Select 0 (Low) for TB6600 driver. '
                                    'Select 1 (High) for DRV8825/A4988 etc.'
                               )
    stepper_group.add_argument('--stepper-ena-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver enable offline GPIO'
                               )
    stepper_group.add_argument('--stepper-ena-delay',
                               type=int,
                               help='stepper driver enable offline delay (ms) [-1=no release]'
                               )
    stepper_group.add_argument('--stepper-micro-step-select1-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver microstep select pin 1 GPIO [MICROSTEP stepper driver only]'
                               )
    stepper_group.add_argument('--stepper-micro-step-select2-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver microstep select pin 2 GPIO [MICROSTEP stepper driver only]'
                               )
    stepper_group.add_argument('--stepper-micro-step-select3-gpio',
                               type=int,
                               choices=choice.GPIO,
                               help='stepper driver microstep select pin 3 GPIO [MICROSTEP stepper driver only]'
                               )

    rig_group = parser.add_argument_group('rig')
    rig_group.add_argument('--rig',
                           action='store_true',
                           help='enable hamlib rig')
    rig_group.add_argument('--rig-model-id',
                           type=int,
                           help='Hamlib rig model ID'
                           )
    rig_group.add_argument('--rig-pathname',
                           help='Hamlib path name to the libs.device file of the rig'
                           )
    rig_group.add_argument('--rig-timeout',
                           type=int,
                           help='Hamlib timeout (ms)'
                           )
    rig_group.add_argument('--rig-retry',
                           type=int,
                           help='Hamlib retry'
                           )
    rig_group.add_argument('--rig-ptt-type',
                           choices=choice.RigPttType,
                           help='Hamlib Push-To-Talk interface type'
                           )
    rig_group.add_argument('--rig-ptt-pathname',
                           help='Hamlib path name to the libs.device file of the Push-To-Talk'
                           )
    rig_group.add_argument('--rig-ptt-bitnum',
                           type=int,
                           help='Hamlib Push-To-Talk Bit number for CM108 GPIO'
                           )
    rig_group.add_argument('--rig-serial-speed',
                           type=int,
                           choices=choice.RigSerialSpeed,
                           help='Hamlib serial speed'
                           )
    rig_group.add_argument('--rig-serial-data-bits',
                           type=int,
                           choices=choice.RigSerialDataBits,
                           help='Hamlib serial data bits'
                           )
    rig_group.add_argument('--rig-serial-stop-bits',
                           type=int,
                           choices=choice.RigSerialStopBits,
                           help='Hamlib serial stop bits'
                           )
    rig_group.add_argument('--rig-serial-parity',
                           choices=choice.RigSerialParity,
                           help='Hamlib serial parity'
                           )
    rig_group.add_argument('--rig-serial-handshake',
                           choices=choice.RigSerialHandshake,
                           help='Hamlib serial handshake'
                           )
    rig_group.add_argument('--rig-serial-rts-state',
                           choices=choice.RigSerialDtrState,
                           help='Hamlib RTS state'
                           )
    rig_group.add_argument('--rig-serial-dtr-state',
                           choices=choice.RigSerialDtrState,
                           help='Hamlib DTR state'
                           )

    http_group = parser.add_argument_group('webapp')
    http_group.add_argument('--webapp',
                            action='store_true',
                            help='enable the HTTP server')
    http_group.add_argument('--webapp-address',
                            help='HTTP listen address'
                            )
    http_group.add_argument('--webapp-port',
                            type=int,
                            help='HTTP listen port'
                            )

    args = parser.parse_args()

    # ensure arg boolean attributes are always set (default: False)
    for arg_boolean in ['daemon', 'show_rigs', 'trace', 'debug', 'ui', 'stepper', 'rig', 'webapp']:
        setattr(args, arg_boolean, getattr(args, arg_boolean, False))

    if args.show_rigs:
        from libs.device.rig import get_hamlib_model_list

        print(tabulate(get_hamlib_model_list(), tablefmt='psql',
                       headers=['Rig Model ID', 'Manufacturer', 'Model', 'Version', 'Status']))
        sys.exit(0)

    from apps.magloop.application import MagLoopApplication

    app = MagLoopApplication(**vars(args))

    app.init_app(args)
    exit_status = app.start()
    sys.exit(exit_status)
