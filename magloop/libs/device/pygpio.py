########################################################################
# Filename    : pygpio.py
# Description : pygpio global for devices
########################################################################

import atexit

import pigpio

pi = pigpio.pi()


@atexit.register
def unload():
    pi.stop()
