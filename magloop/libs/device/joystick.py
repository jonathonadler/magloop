########################################################################
# Filename    : Joystick.py
# Description : Joystick interface
########################################################################

import logging

import pigpio
from .pygpio import pi

_logger = logging.getLogger(__name__)

PCF8591_MIN = 0
PCF8591_MAX = 255
PCF8591_20_PERCENT = int((PCF8591_MAX - PCF8591_MIN) / 5)
PCF8591_MIN_THRESHOLD = PCF8591_MIN + PCF8591_20_PERCENT
PCF8591_MAX_THRESHOLD = PCF8591_MAX - PCF8591_20_PERCENT


# PCF8591 I2C controller
class PCF8591_I2C(object):
    CMD = 0x40

    def __init__(self, port, address):
        self.port = port
        self.address = address
        self._i2c_handle = None

        _logger.debug('port: {}, address: {}'.format(self.port, self.address))

    def start(self):
        if not pi.connected:
            raise ValueError('Unable top connect to pigpiod')
        self._i2c_handle = pi.i2c_open(self.port, self.address)  # open device

    def stop(self):
        if self._i2c_handle:
            pi.i2c_close(self._i2c_handle)

    def analogRead(self, chn):  # read ADC value
        pi.i2c_write_byte(self._i2c_handle, self.CMD + chn)
        value = pi.i2c_read_byte(self._i2c_handle)
        value = pi.i2c_read_byte(self._i2c_handle)
        return value

    def analogWrite(self, value):
        pi.i2c_write_byte_data(self._i2c_handle, self.CMD, value)
        pass

    def input(self, chn):  # Read PCF8591 one port of the data
        return self.analogRead(chn)

    def output(self, value):  # Write data to PCF8591 one port
        self.analogWrite(value)


# Standardization interface
class Joystick(object):
    def __init__(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def get_x(self):
        pass

    def get_y(self):
        pass

    def get_z(self):
        pass

    def is_up(self):
        pass

    def is_down(self):
        pass

    def is_left(self):
        pass

    def is_right(self):
        pass

    def is_select(self):
        pass


# Joystick via I2C (using PCF8591) and GPIO
class JoystickPCF8591(Joystick):
    def __init__(self, port=1, address=0x48, z_gpio=18, *args, **kwargs):
        super().__init__()
        self.pcf8591 = PCF8591_I2C(port, address)
        self.address = address
        self._z_gpio = z_gpio

        _logger.debug('pcf8591: {}, address: {}, _z_gpio: {}'.format(self.pcf8591, self.address, self._z_gpio))

    def start(self):
        self.pcf8591.start()
        if not pi.connected:
            raise ValueError('Unable top connect to pigpiod')
        pi.set_mode(self._z_gpio, pigpio.INPUT)  # set up Z axis input switch
        pi.set_pull_up_down(self._z_gpio, pigpio.PUD_UP)  # set Z Pin to pull-up mode

        _logger.info('Joystick started: {}'.format(self))

    def stop(self):
        self.pcf8591.stop()

        _logger.info('Joystick stopped: {}'.format(self))

    def get_x(self):
        return self.pcf8591.input(1)

    def get_y(self):
        return self.pcf8591.input(0)

    def get_z(self):
        return pi.read(self._z_gpio)  # read digital quality of axis Z

    def is_up(self):
        try:
            joy_val = self.get_y()
        finally:
            pass

        _logger.trace('val:{:2d} '.format(joy_val))

        return joy_val < PCF8591_MIN_THRESHOLD

    def is_down(self):
        try:
            joy_val = self.get_y()
        finally:
            pass

        _logger.trace('val:{:2d} '.format(joy_val))

        return joy_val > PCF8591_MAX_THRESHOLD

    def is_left(self):
        try:
            joy_val = self.get_x()
        finally:
            pass

        _logger.trace('val:{:2d} '.format(joy_val))

        return joy_val < PCF8591_MIN_THRESHOLD

    def is_right(self):
        try:
            joy_val = self.get_x()
        finally:
            pass

        _logger.trace('val:{:2d} '.format(joy_val))

        return joy_val > PCF8591_MAX_THRESHOLD

    def is_select(self):
        try:
            joy_val = self.get_z()
        finally:
            pass

        _logger.trace('val:{:2d} '.format(joy_val))

        return joy_val == 0
