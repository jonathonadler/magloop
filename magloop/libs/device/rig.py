########################################################################
# Filename    : rig.py
# Description : rig control via hamlib
########################################################################

import collections
import logging

_logger = logging.getLogger(__name__)

try:
    import Hamlib

    have_hamlib = True
except ImportError:
    have_hamlib = False

RigStatus = collections.namedtuple('RigStatus', 'code text is_error')

if have_hamlib:
    HAMLIB_RIG_VFOS = [
        Hamlib.RIG_VFO_CURR,
        Hamlib.RIG_VFO_MEM,
        Hamlib.RIG_VFO_VFO,
        Hamlib.RIG_VFO_TX,
        Hamlib.RIG_VFO_RX,
        Hamlib.RIG_VFO_MAIN,
        Hamlib.RIG_VFO_SUB,
        Hamlib.RIG_VFO_A,
        Hamlib.RIG_VFO_B,
        Hamlib.RIG_VFO_C,
    ]

    HAMLIB_RIG_MODES = [
        Hamlib.RIG_MODE_AM,
        Hamlib.RIG_MODE_AMS,
        Hamlib.RIG_MODE_CW,
        Hamlib.RIG_MODE_CWR,
        Hamlib.RIG_MODE_DSB,
        Hamlib.RIG_MODE_ECSSLSB,
        Hamlib.RIG_MODE_ECSSUSB,
        Hamlib.RIG_MODE_FAX,
        Hamlib.RIG_MODE_FM,
        Hamlib.RIG_MODE_FMN,
        Hamlib.RIG_MODE_LSB,
        Hamlib.RIG_MODE_PKTFM,
        Hamlib.RIG_MODE_PKTLSB,
        Hamlib.RIG_MODE_PKTUSB,
        Hamlib.RIG_MODE_RTTY,
        Hamlib.RIG_MODE_RTTYR,
        Hamlib.RIG_MODE_SAH,
        Hamlib.RIG_MODE_SAL,
        Hamlib.RIG_MODE_SAM,
        Hamlib.RIG_MODE_USB,
        Hamlib.RIG_MODE_WFM,
        Hamlib.RIG_MODE_PKTAM,
    ]


def get_hamlib_model_list():
    """ Get the list of rig models
    :return: list of Hamlib rig models
    """

    models = []

    if have_hamlib:
        try:
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_NONE)

            for item in dir(Hamlib):
                if item.startswith('RIG_MODEL_'):
                    rigid = getattr(Hamlib, item)  # Look up the model's numerical index in Hamlib's symbol dictionary.
                    rig = Hamlib.Rig(rigid)
                    if rig.this:
                        models.append([rigid, rig.caps.model_name, rig.caps.mfg_name, rig.caps.version,
                                       Hamlib.rig_strstatus(rig.caps.status)])
        except Exception as ex:
            _logger.error('Could not obtain rig models list via Hamlib: {}'.format(ex))
    else:
        _logger.error('Could not import the Hamlib module.')

    models.sort()

    return models


class Rig(object):
    def __init__(self, model_id=Hamlib.RIG_MODEL_DUMMY, pathname=None, timeout=500,
                 retry=0, ptt_type=None, ptt_pathname=None, ptt_bitnum=None,
                 serial_speed=None, serial_data_bits=None, serial_stop_bits=None, serial_parity=None,
                 serial_handshake=None, serial_rts_state=None, serial_dtr_state=None, *args, **kwargs):
        """ Setup radio via Hamlib.
        :arg str model_id: The model of the radio/rig.
        :arg str pathname: The path to the rig (or rig control device).
        :arg int timeout: The hamlib timeout in milliseconds.
        :arg int retry: The hamlib timeout retry count.
        """

        self._rig = None
        self._model_id = model_id
        self._pathname = pathname
        self._timeout = timeout
        self._retry = retry
        self._ptt_type = ptt_type
        self._ptt_pathname = ptt_pathname
        self._ptt_bitnum = ptt_bitnum
        self._serial_speed = serial_speed
        self._serial_data_bits = serial_data_bits
        self._serial_stop_bits = serial_stop_bits
        self._serial_parity = serial_parity
        self._serial_handshake = serial_handshake
        self._serial_rts_state = serial_rts_state
        self._serial_dtr_state = serial_dtr_state

        if not self._pathname:
            _logger.warning('pathname not specified')

        # Get the list of rig models
        models = get_hamlib_model_list()

        m = [row for row in models if row[0] == model_id]
        if not m:
            raise ValueError('Invalid Hamlib model selected.')

    @property
    def model_id(self):
        return self._model_id

    @model_id.setter
    def model_id(self, value):
        self._model_id = value
        pass

    @property
    def pathname(self):
        return self._pathname

    @pathname.setter
    def pathname(self, value):
        self._pathname = value

    def start(self):
        """ Open a communication channel to the radio."""
        if logging.getLogger().isEnabledFor(logging.NOTSET):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_NONE)
        elif logging.getLogger().isEnabledFor(logging.DEBUG):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_TRACE)
        elif logging.getLogger().isEnabledFor(logging.INFO):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_NONE)
        elif logging.getLogger().isEnabledFor(logging.WARNING):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_WARN)
        elif logging.getLogger().isEnabledFor(logging.ERROR):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_ERR)
        elif logging.getLogger().isEnabledFor(logging.CRITICAL):
            Hamlib.rig_set_debug(Hamlib.RIG_DEBUG_BUG)

        # C API can be seen in action at: https://github.com/Hamlib/Hamlib/blob/master/tests/dumpcaps.c

        self._rig = Hamlib.Rig(self._model_id)  # Look up the model's numerical index in Hamlib's symbol dictionary.

        self.hamlib_model_name = self._rig.caps.model_name
        self.hamlib_mfg_name = self._rig.caps.mfg_name
        self.hamlib_version = self._rig.caps.version
        self.hamlib_status = Hamlib.rig_strstatus(self._rig.caps.status)

        _logger.debug('hamlib_model_name: {}'.format(self.hamlib_model_name))
        _logger.debug('hamlib_mfg_name: {}'.format(self.hamlib_mfg_name))
        _logger.debug('hamlib_version: {}'.format(self.hamlib_version))
        _logger.debug('hamlib_status: {}'.format(self.hamlib_status))

        self.can_set_freq = self._rig.caps.set_freq is not None
        self.can_get_freq = self._rig.caps.get_freq is not None
        self.can_set_mode = self._rig.caps.set_mode is not None
        self.can_get_mode = self._rig.caps.get_mode is not None
        self.can_set_rf_power = self._rig.caps.set_level is not None
        self.can_get_rf_power = self._rig.caps.get_level is not None
        self.can_set_vfo = self._rig.caps.set_vfo is not None
        self.can_get_vfo = self._rig.caps.get_vfo is not None
        self.can_set_ptt = self._rig.caps.set_ptt is not None
        self.can_get_ptt = self._rig.caps.get_ptt is not None

        _logger.debug('Can set Frequency: {}'.format(self.can_set_freq))
        _logger.debug('Can get Frequency: {}'.format(self.can_get_freq))
        _logger.debug('Can set Mode: {}'.format(self.can_set_mode))
        _logger.debug('Can get Mode: {}'.format(self.can_get_mode))
        _logger.debug('Can set RF Power: {}'.format(self.can_set_rf_power))
        _logger.debug('Can get RF Power: {}'.format(self.can_get_rf_power))
        _logger.debug('Can set VFO: {}'.format(self.can_set_vfo))
        _logger.debug('Can get VFO: {}'.format(self.can_get_vfo))
        _logger.debug('Can set PTT: {}'.format(self.can_set_ptt))
        _logger.debug('Can get PTT: {}'.format(self.can_get_ptt))

        if self._pathname:
            self._rig.set_conf('rig_pathname', self._pathname)
        self._rig.set_conf('timeout', '{}'.format(self._timeout))
        self._rig.set_conf('retry', '{}'.format(self._retry))

        if self._ptt_type:
            self._rig.set_conf('ptt_type', '{}'.format(self._ptt_type))
        if self._ptt_pathname:
            self._rig.set_conf('ptt_pathname', '{}'.format(self._ptt_pathname))
        if self._ptt_bitnum:
            self._rig.set_conf('ptt_bitnum', '{}'.format(self._ptt_bitnum))
        if self._serial_speed:
            self._rig.set_conf('serial_speed', '{}'.format(self._serial_speed))
        if self._serial_data_bits:
            self._rig.set_conf('data_bits', '{}'.format(self._serial_data_bits))
        if self._serial_stop_bits:
            self._rig.set_conf('stop_bits', '{}'.format(self._serial_stop_bits))
        if self._serial_parity:
            self._rig.set_conf('serial_parity', '{}'.format(self._serial_parity))
        if self._serial_handshake:
            self._rig.set_conf('serial_handshake', '{}'.format(self._serial_handshake))
        if self._serial_rts_state:
            self._rig.set_conf('rts_state', '{}'.format(self._serial_rts_state))
        if self._serial_dtr_state:
            self._rig.set_conf('dtr_state', '{}'.format(self._serial_dtr_state))

        _logger.debug('pathname: {}'.format(self._pathname))
        _logger.debug('timeout: {}'.format(self._timeout))
        _logger.debug('retry: {}'.format(self._retry))
        _logger.debug('ptt_type: {}'.format(self._ptt_type))
        _logger.debug('ptt_pathname: {}'.format(self._ptt_pathname))
        _logger.debug('serial_speed: {}'.format(self._serial_speed))
        _logger.debug('serial_data_bits: {}'.format(self._serial_data_bits))
        _logger.debug('serial_stop_bits: {}'.format(self._serial_stop_bits))
        _logger.debug('serial_parity: {}'.format(self._serial_parity))
        _logger.debug('serial_handshake: {}'.format(self._serial_handshake))
        _logger.debug('serial_rts_state: {}'.format(self._serial_rts_state))
        _logger.debug('serial_dtr_state: {}'.format(self._serial_dtr_state))

        hamlib_mode_list = self._rig.state.mode_list
        str_modes = list(map(lambda v: Hamlib.rig_strrmode(v), HAMLIB_RIG_MODES))

        self._modes_all = dict(zip(HAMLIB_RIG_MODES, str_modes))
        self._modes_all_reverse = dict([reversed(item) for item in self._modes_all.items()])
        self.modes = sorted([v for k, v in self._modes_all.items() if v])
        self.modes_available = sorted([v for k, v in self._modes_all.items() if k & hamlib_mode_list and v])

        hamlib_vfo_list = self._rig.state.vfo_list
        str_vfos = list(map(lambda v: Hamlib.rig_strvfo(v), HAMLIB_RIG_VFOS))

        self._vfos_all = dict(zip(HAMLIB_RIG_VFOS, str_vfos))
        self._vfos_all_reverse = dict([reversed(item) for item in self._vfos_all.items()])
        self.vfos = sorted([v for k, v in self._vfos_all.items() if v])
        self.vfos_available = sorted([v for k, v in self._vfos_all.items() if k & hamlib_vfo_list and v])

        _logger.debug('Modes All: {} Available: {}'.format(self.modes, self.modes_available))
        _logger.debug('VFOs All: {} Available: {}'.format(self.vfos, self.vfos_available))

        self.open()

        _logger.info('Rig started: {}'.format(self))

    def stop(self):
        """ Close a communication channel to the radio."""
        self.close()

        _logger.info('Rig stopped: {}'.format(self))

    def open(self):
        if self._rig:
            self._rig.open()

    def close(self):
        if self._rig:
            self._rig.close()

    @property
    def status(self):
        """ Returns current rig status
        """
        error_status = self._rig.error_status
        text = Hamlib.rigerror(error_status)

        return RigStatus(code=error_status, text=text, is_error=error_status < 0)

    def _check_status(self):
        rig_status = self.status
        if rig_status.is_error:
            raise RuntimeError('{} - {}'.format(rig_status.code, rig_status.text))

    @staticmethod
    def _check_value(value):
        if value is None:
            raise RuntimeError('Undefined value.')

    @property
    def freq(self):
        try:
            frequency = self._rig.get_freq()
            self._check_status()
            self._check_value(frequency)

            _logger.debug('frequency: {}'.format(frequency))

            return frequency
        except Exception as ex:
            _logger.error('Could not obtain the current frequency via Hamlib: {}'.format(ex))
            raise ex

    @property
    def freq_mhz(self):
        # Convert to the desired unit, if necessary.
        return Rig.convert_frequency(self.freq, from_unit='Hz', to_unit='MHz')

    @freq.setter
    def freq(self, frequency, frequency_unit='Hz'):
        try:
            # Convert to the desired unit, if necessary.
            if frequency_unit != 'Hz':
                frequency = Rig.convert_frequency(frequency, from_unit=frequency_unit, to_unit='Hz')

            _logger.debug('frequency: {}'.format(frequency))

            self._rig.set_freq(Hamlib.RIG_VFO_CURR, int(frequency))
            self._check_status()
        except Exception as ex:
            _logger.error('Could not set the current frequency via Hamlib: {}'.format(ex))
            raise ex

    @property
    def mode(self):
        try:
            (mode, width) = self._rig.get_mode()
            self._check_status()
            self._check_value(mode)

            mode = self._modes_all.get(mode)

            _logger.debug('mode: {}'.format(mode))

            return mode
        except Exception as ex:
            _logger.error('Could not obtain the current mode (e.g. FM, AM, CW) via Hamlib: {}'.format(ex))
            raise ex

    @mode.setter
    def mode(self, mode):
        try:
            _logger.debug('mode: {}'.format(mode))

            self._rig.set_mode(self._modes_all_reverse.get(mode))
            self._check_status()
        except Exception as ex:
            _logger.error('Could not set the current mode (e.g. FM, AM, CW) via Hamlib: {}'.format(ex))
            raise ex

    @property
    def rf_power(self):
        try:
            rfpower = round(self._rig.get_level_f(Hamlib.RIG_LEVEL_RFPOWER) * 100)
            self._check_status()
            self._check_value(rfpower)

            _logger.debug('rfpower:{:d}%'.format(rfpower))

            return rfpower
        except Exception as ex:
            _logger.error('Could not obtain the RF power via Hamlib: {}'.format(ex))
            raise ex

    @rf_power.setter
    def rf_power(self, rfpower):
        try:
            _logger.debug('rfpower:{:d}%'.format(rfpower))

            self._rig.set_level(Hamlib.RIG_LEVEL_RFPOWER, rfpower / 100)
            self._check_status()
        except Exception as ex:
            _logger.error('Could not set the RF power via Hamlib: {}'.format(ex))
            raise ex

    @property
    def vfo(self):
        try:
            vfo = self._vfos_all.get(self._rig.get_vfo())
            self._check_status()
            self._check_value(vfo)

            _logger.debug('vfo: {}'.format(vfo))

            return vfo
        except Exception as ex:
            _logger.error('Could not obtain the VFO via Hamlib: {}'.format(ex))
            raise ex

    @vfo.setter
    def vfo(self, vfo):
        try:
            _logger.debug('vfo: {}'.format(vfo))

            self._rig.set_vfo(self._vfos_all_reverse.get(vfo))
            self._check_status()
        except Exception as ex:
            _logger.error('Could not set the VFO via Hamlib: {}'.format(ex))
            raise ex

    @property
    def ptt(self):
        try:
            ptt = self._rig.get_ptt()
            self._check_status()
            self._check_value(ptt)

            _logger.debug('ptt: {}'.format(ptt))

            return bool(ptt == Hamlib.RIG_PTT_ON)
        except Exception as ex:
            _logger.error('Could not obtain the PTT via Hamlib: {}'.format(ex))
            raise ex

    @ptt.setter
    def ptt(self, ptt):
        try:
            _logger.debug('ptt: {}'.format(ptt))

            self._rig.set_ptt(Hamlib.RIG_VFO_CURR, Hamlib.RIG_PTT_ON if ptt else Hamlib.RIG_PTT_OFF)
            self._check_status()
        except Exception as ex:
            _logger.error('Could not set the PTT via Hamlib: {}'.format(ex))
            raise ex

    @staticmethod
    def convert_frequency(frequency: float, from_unit: str, to_unit: str) -> float:
        """ Convert a frequency from one unit to another.
        :arg float frequency: The frequency to convert.
        :arg str from_unit: The current unit of the frequency.
        :arg str to_unit: The desired unit of the frequency.
        :rtype: float
        :returns: The frequency in the to_unit.
        """

        scaling = {'Hz': 1, 'kHz': 1e3, 'MHz': 1e6, 'GHz': 1e9}
        # Check that the from/to frequency units are valid.
        try:
            if from_unit not in scaling.keys():
                raise ValueError('Unknown frequency unit ''%s'' in from_unit' % from_unit)
            if to_unit not in scaling.keys():
                raise ValueError('Unknown frequency unit ''%s'' in to_unit' % to_unit)
        except ValueError as ex:
            _logger.error('Could not convert frequency {}'.format(ex))
            return frequency
        # Cast to float before scaling.
        if not isinstance(frequency, float):
            try:
                if frequency == '' or frequency is None:
                    return -1
                else:
                    frequency = float(frequency)
            except (ValueError, TypeError) as ex:
                _logger.error('Could not convert frequency to a floating-point value. {}'.format(ex))
                return frequency
        # Do not bother scaling if the units are the same.
        if from_unit == to_unit:
            return frequency

        coefficient = scaling[from_unit] / scaling[to_unit]
        return float('%.6f' % (coefficient * frequency))
