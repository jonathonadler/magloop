########################################################################
# Filename    : stepper.py
# Description : Stepper Motor interface (TB6600/DRV8825)
########################################################################
import collections
import logging
import math

import gevent
import pigpio
from gevent.event import Event

from .pygpio import pi

_logger = logging.getLogger(__name__)

_MicroStep = collections.namedtuple('MicroStep', 'steps_per_rev std_freq min_freq max_freq acceleration')
_Block = collections.namedtuple('Block',
                                'acceleration accelerate_until decelerate_after plateau_steps initial_freq final_freq max_freq')

DIRECTION_CW = 1  # Clockwise Rotation
DIRECTION_CCW = 0  # Counterclockwise Rotation

ENABLE_HIGH = 1
ENABLE_LOW = 0

MICRO_STEP = {
    # micro steps used to set the number of pulses per revolution (assuming 1.8 degrees per pulse)
    1: _MicroStep(steps_per_rev=200, std_freq=100, min_freq=1, max_freq=200, acceleration=200),
    2: _MicroStep(steps_per_rev=400, std_freq=200, min_freq=1, max_freq=400, acceleration=400),
    4: _MicroStep(steps_per_rev=800, std_freq=400, min_freq=1, max_freq=800, acceleration=800),
    8: _MicroStep(steps_per_rev=1600, std_freq=800, min_freq=1, max_freq=1600, acceleration=1600),
    16: _MicroStep(steps_per_rev=3200, std_freq=1600, min_freq=1, max_freq=3200, acceleration=3200),
    32: _MicroStep(steps_per_rev=6400, std_freq=3200, min_freq=1, max_freq=6400, acceleration=6400),
}

MICRO_STEP_SELECT_PIN_LEVELS = {
    # micro steps select pin levels used by DRV8825/A4988 to set the number of pulses per revolution (assuming 1.8 degrees per pulse)
    1: (0, 0, 0),
    2: (1, 0, 0),
    4: (0, 1, 0),
    8: (1, 1, 0),
    16: (0, 0, 1),
    32: (1, 0, 1),
}

Step = collections.namedtuple('Step', 'direction steps freq')
StepAcceleration = collections.namedtuple('StepAcceleration', 'direction steps min_freq max_freq acceleration')


class _Stepper(object):
    def __init__(self, dir_gpio=None, pul_gpio=None, micro_step=None):

        if not dir_gpio:
            raise ValueError('dir_gpio must be specified')
        if not pul_gpio:
            raise ValueError('pul_gpio must be specified')
        if not micro_step:
            raise ValueError('micro_step must be specified')

        self.dir_gpio = dir_gpio
        self.pul_gpio = pul_gpio
        self.micro_step = micro_step
        self.__position = 0
        self.__busy_event = Event()
        self.__cancel_event = Event()
        self.__step_greenlet = None
        self.__position_cb_ref = None

    def start(self):
        if not pi.connected:
            raise ValueError('Unable top connect to pigpiod')

        # Set up pins as output
        pi.set_mode(self.dir_gpio, pigpio.OUTPUT)
        pi.set_mode(self.pul_gpio, pigpio.OUTPUT)

        _logger.debug('dir_gpio: {}, pul_gpio: {}'.format(self.dir_gpio, self.pul_gpio))

        self.__position_cb_ref = pi.callback(self.pul_gpio, pigpio.RISING_EDGE, self.__cb_position)
        self.__busy_event.clear()
        self.cancelled = False

    def stop(self):
        self.cancelled = True

        if self.__step_greenlet:
            self.__step_greenlet.join()

        if self.__position_cb_ref:
            self.__position_cb_ref.cancel()

    def __cb_position(self, gpio, level, tick):
        current_dir = pi.read(self.dir_gpio)
        self.__position += 1 if current_dir else -1

    @property
    def busy(self):
        return self.__busy_event.is_set()

    @property
    def cancelled(self):
        return self.__cancel_event.is_set()

    @cancelled.setter
    def cancelled(self, val):
        _logger.debug('cancelled:{!s} '.format(val))
        if val:
            self.__cancel_event.set()
        else:
            self.__cancel_event.clear()

    @property
    def position(self):
        return self.__position

    @position.setter
    def position(self, position):
        direction = DIRECTION_CCW if position < self.position else DIRECTION_CW
        new_position = abs(position - self.position)

        _logger.debug('position:{!s} '.format(position))

        self.step([StepAcceleration(direction=direction, steps=new_position, min_freq=None, max_freq=None,
                                    acceleration=None)])

    def step(self, steps=None):
        """
        Step the stepper motor.

        :param steps: [Step[direction, steps, freq], StepAcceleration[direction, steps, min_freq, max_freq, acceleration], ...]
        :return:
        """
        if steps is None:
            steps = []

        try:
            if self.__step_greenlet:
                gevent.kill(self.__step_greenlet)

            self.__step_greenlet = gevent.spawn(self._step_function, steps)
        except Exception:
            _logger.exception('step error')

    def _step_function(self, steps):

        wave_ids = []
        chain = []

        try:
            self.__busy_event.set()
            self.cancelled = False

            pi.wave_clear()  # delete all waves

            for e in steps:
                # step with a fixed frequency
                if isinstance(e, Step):
                    frequency = e.freq if e.freq else MICRO_STEP[self.micro_step].std_freq

                    if frequency <= 0:
                        raise ValueError('frequency must be > 0')

                    _logger.debug('direction:{!s}, steps:{!s}, frequency:{!s}'.format(e.direction, e.steps, frequency))

                    # create stepper motor pulse waveform
                    waveform = []

                    # create stepper motor direction waveform
                    if e.direction:
                        waveform.append(pigpio.pulse(1 << self.dir_gpio, 0, 0))
                    else:
                        waveform.append(pigpio.pulse(0, 1 << self.dir_gpio, 0))

                    waveform.append(pigpio.pulse(0, 1 << self.pul_gpio, int((1000000 / frequency) / 2)))
                    waveform.append(pigpio.pulse(1 << self.pul_gpio, 0, int((1000000 / frequency) / 2)))

                    pi.wave_add_generic(waveform)

                    wave_id = pi.wave_create()
                    wave_ids.append(wave_id)

                    _logger.debug('create wave_id: {} '.format(wave_id))

                    # generate chain
                    x = e.steps & 255
                    y = e.steps >> 8
                    c = [255, 0, wave_id, 255, 1, x, y]

                    _logger.debug('chain:{!s} '.format(c))

                    chain += c

                # step with variable frequency (accelerate, plateau and decelerate)
                elif isinstance(e, StepAcceleration):
                    min_freq = e.min_freq if e.min_freq else MICRO_STEP[self.micro_step].min_freq
                    max_freq = e.max_freq if e.max_freq else MICRO_STEP[self.micro_step].max_freq
                    acceleration = e.acceleration if e.acceleration else MICRO_STEP[self.micro_step].acceleration

                    if min_freq <= 0:
                        raise ValueError('min_freq must be > 0')
                    if max_freq <= 0:
                        raise ValueError('max_freq must be > 0')
                    if acceleration <= 0:
                        raise ValueError('acceleration must be > 0')

                    _logger.debug('direction:{!s}, steps:{!s}, min_freq:{!s}, max_freq:{!s}, acceleration:{!s}'.format(
                        e.direction, e.steps, min_freq, max_freq, acceleration))

                    block = calculate_block(e.steps, min_freq, max_freq, acceleration)

                    # calculate block parts
                    part1_freq = []
                    part1_delay = []
                    part3_freq = []
                    part3_delay = []

                    for i in range(block.accelerate_until):
                        freq = math.sqrt((block.initial_freq * block.initial_freq) + (2 * block.acceleration * (i + 1)))
                        if freq < block.initial_freq:
                            freq = block.initial_freq
                        elif freq > block.max_freq:
                            freq = block.max_freq

                        part1_freq.append(freq)
                        part1_delay.append(int((1000000 / freq) / 2))

                    part2_delay = int(1000000 / block.max_freq) / 2

                    for i in range(e.steps - block.decelerate_after):
                        freq = math.sqrt((block.max_freq * block.max_freq) - (2 * block.acceleration * (i + 1)))
                        if freq < block.final_freq:
                            freq = block.final_freq
                        elif freq > block.max_freq:
                            freq = block.max_freq

                        part3_freq.append(freq)
                        part3_delay.append(int((1000000 / freq) / 2))

                    max_pulses = int((600 / 2) - 1)  # maximum of 600 pulses per chain, we add 2 pulses and a direction

                    # acceleration
                    start = 0
                    end = min(max_pulses, start + block.accelerate_until)

                    _logger.debug('accelerate start: {}, end: {} '.format(start, end))

                    # break up the acceleration to not exceed the limits for standard pigpio chains
                    while True:
                        # create stepper motor pulse waveform
                        waveform = []

                        # create stepper motor direction waveform
                        if e.direction:
                            waveform.append(pigpio.pulse(1 << self.dir_gpio, 0, 0))
                        else:
                            waveform.append(pigpio.pulse(0, 1 << self.dir_gpio, 0))

                        if start <= end:
                            for i in range(start, end):  # range executes for start to ** end-1 **
                                waveform.append(pigpio.pulse(0, 1 << self.pul_gpio, int(part1_delay[i])))
                                waveform.append(pigpio.pulse(1 << self.pul_gpio, 0, int(part1_delay[i])))

                            start = start + max_pulses
                            end = min((end + max_pulses), block.accelerate_until)

                            _logger.debug('accelerate start: {}, end: {} '.format(start, end))

                            pi.wave_add_generic(waveform)

                            wave_id = pi.wave_create()
                            wave_ids.append(wave_id)

                            _logger.debug('create wave_id: {} '.format(wave_id))

                            chain.append(wave_id)
                        else:
                            break

                    # plateau
                    waveform = []

                    # create stepper motor direction waveform
                    if e.direction:
                        waveform.append(pigpio.pulse(1 << self.dir_gpio, 0, 0))
                    else:
                        waveform.append(pigpio.pulse(0, 1 << self.dir_gpio, 0))

                    waveform.append(pigpio.pulse(0, 1 << self.pul_gpio, int(part2_delay)))
                    waveform.append(pigpio.pulse(1 << self.pul_gpio, 0, int(part2_delay)))

                    pi.wave_add_generic(waveform)

                    wave_id = pi.wave_create()
                    wave_ids.append(wave_id)

                    _logger.debug('create wave_id: {} '.format(wave_id))

                    x = block.plateau_steps & 255
                    y = block.plateau_steps >> 8
                    c = [255, 0, wave_id, 255, 1, x, y]

                    _logger.debug('chain:{!s} '.format(c))

                    chain += c

                    # deceleration
                    start = 0
                    end = min(max_pulses, e.steps - block.decelerate_after)

                    _logger.debug('deccelerate start: {}, end: {} '.format(start, end))

                    # break up the acceleration to not exceed the limits for standard pigpio chains
                    while True:
                        # create stepper motor pulse waveform
                        waveform = []

                        # create stepper motor direction waveform
                        if e.direction:
                            waveform.append(pigpio.pulse(1 << self.dir_gpio, 0, 0))
                        else:
                            waveform.append(pigpio.pulse(0, 1 << self.dir_gpio, 0))

                        if start <= end:
                            for i in range(start, end):  # range executes for start to ** end-1 **,
                                waveform.append(pigpio.pulse(0, 1 << self.pul_gpio, int(part3_delay[i])))
                                waveform.append(pigpio.pulse(1 << self.pul_gpio, 0, int(part3_delay[i])))

                            start = start + max_pulses
                            end = min((end + max_pulses), e.steps - block.decelerate_after)

                            _logger.debug('deccelerate start: {}, end: {} '.format(start, end))

                            pi.wave_add_generic(waveform)

                            wave_id = pi.wave_create()
                            wave_ids.append(wave_id)

                            _logger.debug('create wave_id: {} '.format(wave_id))

                            chain.append(wave_id)
                        else:
                            break

            _logger.debug('wave_chain:{!s} '.format(chain))

            pi.wave_chain(chain)  # transmit chain

            while pi.wave_tx_busy() and not self.cancelled:  # while transmitting
                gevent.sleep(0.01)

        except Exception:
            _logger.exception('step error')

        finally:
            if self.cancelled:
                pi.wave_tx_stop()

            for wave_id in wave_ids:
                ret = pi.wave_delete(wave_id)
                _logger.debug('delete wave_id: {}, return: {}'.format(wave_id, ret))

            self.__busy_event.clear()

    @property
    def direction(self):
        return pi.read(self.dir_gpio)

    @direction.setter
    def direction(self, val):
        _logger.debug('direction:{!s} '.format(val))
        pi.write(self.dir_gpio, 1 if val else 0)


class _StepperEnable(_Stepper):
    def __init__(self, dir_gpio=None, pul_gpio=None, micro_step=None,
                 ena_offline=None, ena_gpio=None, ena_delay=None):
        super().__init__(dir_gpio=dir_gpio, pul_gpio=pul_gpio, micro_step=micro_step)

        if not ena_gpio:
            raise ValueError('ena_gpio must be specified')
        if not ena_delay:
            raise ValueError('ena_delay must be specified')

        self.ena_offline = ENABLE_HIGH if ena_offline else ENABLE_LOW
        self.ena_gpio = ena_gpio
        self.enable_delay_ms = ena_delay

    def start(self):
        try:
            super().start()

            # Set up pins as output
            pi.set_mode(self.ena_gpio, pigpio.OUTPUT)

            _logger.debug('ena_offline: {}, ena_gpio: {}, enable_delay_ms: {}'.format(self.ena_offline,
                                                                                      self.ena_gpio,
                                                                                      self.enable_delay_ms))

            _logger.info('Stepper started: {}'.format(self))
        except Exception:
            _logger.exception('StepperEnable start error')

    def stop(self):
        try:
            self.enable = True

            super().stop()

            _logger.info('Stepper stopped: {}'.format(self))
        except Exception:
            _logger.exception('StepperEnable stop error')

    def _step_function(self, steps=None):
        if steps is None:
            steps = []
        self.enable = False

        super()._step_function(steps)

        if self.enable_delay_ms >= 0:
            if not self.cancelled:
                gevent.sleep(self.enable_delay_ms / 1000)

            self.enable = True

    @property
    def enable(self):
        return pi.read(self.ena_gpio) == self.ena_offline

    @enable.setter
    def enable(self, val):
        ena_pin = self.ena_offline if val else int(not self.ena_offline)
        _logger.debug('enable:{!s}, ena_pin: {}'.format(val, ena_pin))
        pi.write(self.ena_gpio, ena_pin)


class StepperGeneric(_StepperEnable):
    """
    Stepper motor driver controller for driver with Direction, Pulse and Enable pins (eg. TB6600)
    """

    def __init__(self, dir_gpio=None, pul_gpio=None, micro_step=1,
                 ena_offline=None, ena_gpio=None, ena_delay=None,
                 *args, **kwargs):
        super().__init__(dir_gpio=dir_gpio, pul_gpio=pul_gpio, micro_step=micro_step,
                         ena_offline=ena_offline, ena_gpio=ena_gpio, ena_delay=ena_delay)


class StepperMicrostep(_StepperEnable):
    """
    Stepper motor driver controller for driver with Direction, Pulse, Enable and Microstep Select pins (eg. DRV8825/A4988)
    """

    def __init__(self, dir_gpio=None, pul_gpio=None, micro_step=1,
                 ena_offline=None, ena_gpio=None, ena_delay=None,
                 micro_step_select1_gpio=None, micro_step_select2_gpio=None, micro_step_select3_gpio=None,
                 *args, **kwargs):
        super().__init__(dir_gpio=dir_gpio, pul_gpio=pul_gpio, micro_step=micro_step,
                         ena_offline=ena_offline, ena_gpio=ena_gpio, ena_delay=ena_delay)

        if not micro_step_select1_gpio:
            raise ValueError('micro_step_select1_gpio must be specified')
        if not micro_step_select2_gpio:
            raise ValueError('micro_step_select2_gpio must be specified')
        if not micro_step_select3_gpio:
            raise ValueError('micro_step_select3_gpio must be specified')

        self.micro_step_select1_gpio = micro_step_select1_gpio
        self.micro_step_select2_gpio = micro_step_select2_gpio
        self.micro_step_select3_gpio = micro_step_select3_gpio

        self.__micro_step_mode_levels = MICRO_STEP_SELECT_PIN_LEVELS[self.micro_step]

    def start(self):
        try:
            super().start()

            # Set up pins as output
            pi.set_mode(self.micro_step_select1_gpio, pigpio.OUTPUT)
            pi.set_mode(self.micro_step_select2_gpio, pigpio.OUTPUT)
            pi.set_mode(self.micro_step_select3_gpio, pigpio.OUTPUT)

            _logger.debug(
                'micro_step_select1_gpio: {}, micro_step_select2_gpio: {}, micro_step_select3_gpio: {}'.format(
                    self.micro_step_select1_gpio, self.micro_step_select2_gpio, self.micro_step_select3_gpio))

            (m0_level, m1_level, m2_level) = self.__micro_step_mode_levels

            pi.write(self.micro_step_select1_gpio, m0_level)
            pi.write(self.micro_step_select2_gpio, m1_level)
            pi.write(self.micro_step_select3_gpio, m2_level)

            _logger.debug('m0_level: {}, m1_level: {}, m2_level: {}'.format(m0_level, m1_level, m2_level))
        except Exception:
            _logger.exception('StepperMicrostep start error')


def calculate_block(steps, min_freq, max_freq, acceleration):
    initial_freq = final_freq = min_freq

    accelerate_steps = math.ceil(estimate_acceleration_distance(initial_freq, max_freq, acceleration))
    decelerate_steps = math.floor(estimate_acceleration_distance(max_freq, final_freq, -acceleration))

    # Calculate the size of Plateau of Nominal Rate.
    plateau_steps = steps - accelerate_steps - decelerate_steps

    # Is the Plateau of Nominal Rate smaller than nothing? That means no cruising, and we will
    # have to use intersection_distance() to calculate when to abort acceleration and start braking
    # in order to reach the final_freq exactly at the end of this block.
    if plateau_steps < 0:
        accelerate_steps = math.ceil(intersection_distance(initial_freq, final_freq, acceleration, steps))
        accelerate_steps = max(accelerate_steps, 0)  # Check limits due to numerical round-off
        accelerate_steps = min(accelerate_steps, steps)  # above line ensures that we are above zero
        plateau_steps = 0

    return _Block(acceleration=acceleration, accelerate_until=accelerate_steps,
                  decelerate_after=accelerate_steps + plateau_steps, plateau_steps=plateau_steps,
                  initial_freq=initial_freq, final_freq=final_freq, max_freq=max_freq)


# Calculate the distance it takes to accelerate from initial_freq to target_rate using the given acceleration
def estimate_acceleration_distance(initial_freq, target_rate, acceleration):
    if acceleration != 0:
        return ((target_rate * target_rate) - (initial_freq * initial_freq)) / (2.0 * acceleration)
    else:
        return 0.0  # acceleration was 0, set acceleration distance to 0


# Returns the point at which you must start braking (at the rate of -acceleration) if # you started at speed
# initial_freq and accelerated until this point and want to end at the final_freq after a total travel of distance.
# This can be used to compute the intersection point between acceleration and deceleration in the cases where the
# trapezoid has no plateau (i.e. never reaches maximum speed)
def intersection_distance(initial_freq, final_freq, acceleration, distance):
    if acceleration != 0:
        return ((2.0 * acceleration * distance) - (initial_freq * initial_freq) + (final_freq * final_freq)) / (
            4.0 * acceleration)
    else:
        return 0.0  # acceleration was 0, set intersection distance to 0
