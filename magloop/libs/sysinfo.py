########################################################################
# Filename    : sysinfo.py
# Description : System information
########################################################################

import logging
import os
import socket
from datetime import datetime

import psutil

_logger = logging.getLogger(__name__)


def bytes2human(n):
    # http://code.activestate.com/recipes/578019
    # >>> bytes2human(10000)
    # '9.8K'
    # >>> bytes2human(100001221)
    # '95.4M'
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n


# get CPU temperature
def get_cpu_temp():
    tmp = open('/sys/class/thermal/thermal_zone0/temp')
    cpu = tmp.read()
    tmp.close()
    return '{:4.0f}C'.format(float(cpu) / 1000)


# get CPU utilisation
def get_cpu_utilisation():
    return '{:4.1f}%'.format(psutil.cpu_percent(interval=None))


# get mem free
def get_mem_free():
    return '{!s}'.format(bytes2human(psutil.virtual_memory().available))


# get system date/time
def get_datetime_now():
    d = datetime.now()
    return {'date': d.strftime('%d/%m/%y'), 'time': d.strftime('%H:%M:%S')}


# get ipv4
def _get_interface_ipv4(ifname):
    return os.popen('/bin/ip addr show {}'.format(ifname)).read().split('inet ')[1].split('/')[0]


# get ipv6
def _get_interface_ipv6(ifname):
    return os.popen('/bin/ip addr show {}'.format(ifname)).read().split('inet6 ')[1].split('/')[0]


# get ipv4/ipv6
def get_interface_ip():
    ipv4 = ipv6 = 'unknown'

    ip = socket.gethostbyname(socket.gethostname())
    if ip.startswith('127.'):
        interfaces = [
            'eth0',
            'wlan0',
            'wifi0',
        ]
        for ifname in interfaces:
            try:
                ipv4 = _get_interface_ipv4(ifname)
                ipv6 = _get_interface_ipv6(ifname)
                break
            except (IOError, IndexError):
                pass
    return {'ipv4': ipv4, 'ipv6': ipv6}


# get hostname
def get_hostname():
    return socket.gethostname()
