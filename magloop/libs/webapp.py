########################################################################
# Filename    : webapp.py
# Description : Web app
########################################################################

import logging

from gevent.pywsgi import WSGIServer

_logger = logging.getLogger(__name__)


class WebApp():
    def __init__(self, address='127.0.0.1', port=8080, application=None, *args, **kwargs):
        if not application:
            raise ValueError('application must be specified')

        self.address = address
        self.port = port
        self.application = application

        self.wsgi_server = WSGIServer((self.address, self.port),
                                      application=self.application,
                                      log=_logger,
                                      error_log=_logger)

    def start(self):
        self.wsgi_server.start()
        _logger.info('WebApp started: {}'.format(self.wsgi_server))

    def stop(self):
        self.wsgi_server.stop()
        _logger.info('WebApp stopped: {}'.format(self.wsgi_server))
