########################################################################
# Filename    : menu.py
# Description : Navigation menu optimised for LCD screens
########################################################################

import logging

_logger = logging.getLogger(__name__)

MENU_REFRESH_FAST = 20
MENU_REFRESH_NORMAL = 100
MENU_REFRESH_SLOW = 200


class _Menu():
    def __init__(self, title=None, text=None, cols=16, rows=2, parent=None, left=None, right=None, up=None, down=None,
                 nav_left='\x00', nav_right='\x01', nav_up='\x02', nav_down='\x03', nav_up_down='\x04', nav_back='\x05',
                 refresh_ms=MENU_REFRESH_NORMAL):
        self.title = title
        self.text = text
        self.cols = cols
        self.rows = rows
        self.parent = parent
        self.left = left
        self.right = right
        self.up = up
        self.down = down
        self.nav_left = nav_left
        self.nav_right = nav_right
        self.nav_up_down = nav_up_down
        self.nav_up = nav_up
        self.nav_down = nav_down
        self.nav_back = nav_back
        self.refresh_ms = refresh_ms

        if rows < 1 or cols < 1:
            raise ValueError('rows and cols must be greater than 0.')

    def __str__(self):
        return str(self.__dict__)

    def action(self):
        return self

    def left_press(self):
        return self

    def right_press(self):
        return self

    def up_press(self):
        return self

    def down_press(self):
        return self

    def select_press(self):
        return self

    @property
    def available(self):
        return True


class MenuNav(_Menu):
    def __init__(self, menuitems=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not isinstance(menuitems, list):
            raise ValueError('menuitems must be a list of MenuNav or MenuItem.')

        self._menuitems = menuitems

        for i, m in enumerate(self._menuitems):
            if not (isinstance(m, MenuNav) or isinstance(m, MenuItem)):
                raise ValueError('menuitems must be a list of MenuNav or MenuItem.')

        self.initialise()

        _logger.debug('val: {}'.format(self))

    def initialise(self):
        # filter out unavailable menus
        self._menuitems = [item for item in self._menuitems if item.available]

        if self.parent and not isinstance(self._menuitems[-1], MenuNavBack):
            self._menuitems.append(MenuNavBack())

        # link menu navigation together
        for i, m in enumerate(self._menuitems):
            m.parent = self

            # set cols and rows of sub-menus
            m.cols = self.cols
            m.rows = self.rows

            # link menus together
            if i < len(self._menuitems) - 1:
                m.down = self._menuitems[i + 1]
            if i:
                m.up = self._menuitems[i - 1]

            if isinstance(m, MenuNav):
                m.initialise()

        self._selected_menuitem = self._menuitems[0]
        self._nav_ind_index = 0
        self._menu_height = self.rows - (1 if self.title else 0)

    def action(self):
        text_rows = []

        if self.title:
            text_rows.append(
                '{title:{char}^{width}}'.format(title=' {} '.format(self.title), width=self.cols, char='*')
            )

        # print out menu on remaining rows
        selected_menuitem_index = self._menuitems.index(self._selected_menuitem)
        row_index = max(selected_menuitem_index - self._nav_ind_index, 0)

        # logger.debug('selected_menuitem_index: {}, row_index: {}, self._nav_ind_index: {}'.
        #              format(selected_menuitem_index, row_index, self._nav_ind_index))

        while len(text_rows) < self.rows:
            if row_index < len(self._menuitems):
                menuitem = self._menuitems[row_index]

                nav_ind = ''
                if self._menu_height == 1:
                    nav_ind = self.nav_up_down
                elif menuitem == self._selected_menuitem:
                    if isinstance(self._selected_menuitem, MenuNavBack):
                        nav_ind = self.nav_back
                    else:
                        nav_ind = self.nav_right

                text_rows.append('{:1s} {}'.format(nav_ind, menuitem.title))
            else:
                text_rows.append('')

            row_index += 1

        self.text = '\n'.join(text_rows)
        return self

    def left_press(self):
        if self.left:
            self._selected_menuitem = self._menuitems[0]
            return self.left
        return self

    def right_press(self):
        if self.right:
            self._selected_menuitem = self._menuitems[0]
            return self.right
        return self

    def up_press(self):
        if self._selected_menuitem.up:
            if self._nav_ind_index > 0:
                self._nav_ind_index -= 1
            self._selected_menuitem = self._selected_menuitem.up
        return self

    def down_press(self):
        if self._selected_menuitem.down:
            if self._nav_ind_index < self._menu_height - 1:
                self._nav_ind_index += 1
            self._selected_menuitem = self._selected_menuitem.down
        return self

    def select_press(self):
        if isinstance(self._selected_menuitem, MenuNav):
            self._selected_menuitem.initialise()
        return self._selected_menuitem


class MenuItem(_Menu):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.title:
            raise ValueError('title must be specified')

        _logger.debug('val: {} '.format(self))

    def left_press(self):
        if self.left:
            return self.left
        return self

    def right_press(self):
        if self.right:
            return self.right
        return self


class MenuNavBack(MenuItem):
    def __init__(self, title='Back', *args, **kwargs):
        super().__init__(*args, title=title, **kwargs)

    def action(self):
        return self.parent.parent


class MenuItemScrollable(MenuItem):
    def __init__(self, text=None, display_title=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._text = text
        self._display_title = display_title
        self._max_y_offset = 0
        self._y_offset = 0

    @property
    def text(self):
        if not self._text:
            raise ValueError('text must be specified')

        text_rows = self._text.split('\n')[self._y_offset:]
        text = '\n'.join(text_rows)

        if self._display_title:
            scroll_height = self.rows - (1 if self._display_title else 0)

            if self._y_offset > 0 and len(text_rows) > scroll_height:
                scroll = ' ' + self.nav_up_down
            elif len(text_rows) > scroll_height:
                scroll = ' ' + self.nav_down
            elif self._y_offset > 0:
                scroll = ' ' + self.nav_up
            else:
                scroll = ''

            title = '{title:{fill}^{width}}{scroll}'.format(title=' {} '.format(self.title),
                                                            width=self.cols - len(scroll), fill='*', scroll=scroll)
            return '{title}\n{text}'.format(title=title, text=text)

        return text

    @text.setter
    def text(self, val):
        self._text = val

    @property
    def y_offset(self):
        return self._y_offset

    @y_offset.setter
    def y_offset(self, val):
        if self._text:
            self._max_y_offset = len(self._text.split('\n')) - self.rows + (1 if self._display_title else 0)
            if val >= 0 and val <= self._max_y_offset:
                self._y_offset = val

    def up_press(self):
        self.y_offset -= 1
        return self

    def down_press(self):
        self.y_offset += 1
        return self

    def select_press(self):
        self._y_offset = 0
        return self.parent


class MenuItemConfirmation(MenuItem):
    def __init__(self, confirmation_title=None, confirmation_text=None,
                 confirmation_yes='Yes', confirmation_no='No', confirmation_default=False,
                 append_question_mark=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.confirmation_title = confirmation_title
        self.confirmation_text = confirmation_text
        self._confirmation_yes = confirmation_yes
        self._confirmation_no = confirmation_no
        self._is_confirmed = confirmation_default
        self._append_question_mark = append_question_mark

    def action(self):
        title = self.confirmation_title if self.confirmation_title else self.title

        if self._append_question_mark:
            if title[:-1] != '?':
                title += '?'

        text = '{title:{char}^{width}}'.format(title=' {} '.format(title), width=self.cols, char='*')
        scroll_height = self.rows - 1

        if self.confirmation_text:
            text += '\n' + self.confirmation_text
            scroll_height -= len(self.confirmation_text.split('\n'))

        if scroll_height == 1:
            if self._is_confirmed:
                confirmation_value = self._confirmation_yes
            else:
                confirmation_value = self._confirmation_no

            self.text = '{text}\n' \
                        '{nav:1} {val}' \
                .format(text=text, nav=self.nav_up_down, val=confirmation_value)
        else:
            self.text = '{text}\n' \
                        '{nav_y:1} {yes}\n' \
                        '{nav_n:1} {no}' \
                .format(text=text,
                        nav_y=self.nav_right if self._is_confirmed else '', yes=self._confirmation_yes,
                        nav_n=self.nav_right if not self._is_confirmed else '', no=self._confirmation_no)

        return self

    def up_press(self):
        scroll_height = self.rows - 1
        if scroll_height == 1 or not self._is_confirmed:
            self._is_confirmed = not self._is_confirmed
        return self

    def down_press(self):
        scroll_height = self.rows - 1
        if scroll_height == 1 or self._is_confirmed:
            self._is_confirmed = not self._is_confirmed
        return self

    def select_press(self):
        if self._is_confirmed:
            return self.confirmed()
        else:
            return self.parent

    def confirmed(self):
        # overwrite this function when subclassing
        return self
