########################################################################
# Filename    : virtualui.py
# Description : LCD and Joystick abstraction
########################################################################

import logging
import time
from functools import wraps

import gevent

from .menu import MenuNav

_logger = logging.getLogger(__name__)

X_SCROLL_DELAY_SECS = 0.3
X_SCROLL_WAIT_SECS = 2


def button(func):
    @wraps(func)
    def func_wrapper(self):
        ret = 0
        try:
            ret = func(self)
            if ret:
                self.charlcd.backlight = True
        except Exception:
            _logger.exception('button error')
        return ret

    return func_wrapper


class VirtualUI(object):
    def __init__(self, charlcd, joystick):
        self.charlcd = charlcd
        self.joystick = joystick
        self._state = [{'content': '', 'viewport': '', 'xpos': 0, 'xtime': 0} for _ in range(self.charlcd.lcd.rows)]

    def start(self):
        try:
            self.charlcd.start()
        except Exception:
            _logger.exception('charlcd start error')

        try:
            self.joystick.start()
        except Exception:
            _logger.exception('joystick start error')

        _logger.info('VirtualUI started: {}'.format(self))

    def stop(self):
        try:
            self.charlcd.stop()
        except Exception:
            _logger.exception('charlcd stop error')

        try:
            self.joystick.stop()
        except Exception:
            _logger.exception('joystick stop error')

        _logger.info('VirtualUI stopped: {}'.format(self))

    def _scroll_row(self, rownum):
        pass

    def _refresh_row(self, rownum, content):
        state_row = self._state[rownum]

        if content != state_row['content']:
            state_row['content'] = content
            state_row['xpos'] = 0
            state_row['xtime'] = time.perf_counter()
        else:
            xend = state_row['xpos'] + self.charlcd.lcd.cols
            l = len(state_row['content'])

            # if content is too big to fit the viewport
            if state_row['xpos'] == 0 and xend < l:  # scroll on first position
                if time.perf_counter() - state_row['xtime'] > X_SCROLL_WAIT_SECS:
                    state_row['xpos'] += 1
                    state_row['xtime'] = time.perf_counter()
            elif state_row['xpos'] > 0 and xend == l:  # scroll on last position
                if time.perf_counter() - state_row['xtime'] > X_SCROLL_WAIT_SECS:
                    state_row['xpos'] = 0
                    state_row['xtime'] = time.perf_counter()
            elif xend < l:  # scroll to the right
                if time.perf_counter() - state_row['xtime'] > X_SCROLL_DELAY_SECS:
                    state_row['xpos'] += 1
                    state_row['xtime'] = time.perf_counter()
            else:
                state_row['xpos'] = 0
                state_row['xtime'] = time.perf_counter()

        viewport = state_row['content'][state_row['xpos']:self.charlcd.lcd.cols + state_row['xpos']]

        if state_row['viewport'] != viewport:
            _logger.debug('old viewport: {} new viewport: {}'.format(state_row['viewport'], viewport))
            state_row['viewport'] = viewport
            self.charlcd.cursor_pos = (rownum, 0)
            self.charlcd.write_string(viewport)

    def _refresh(self, row_array):
        for i in range(0, self.charlcd.lcd.rows):
            self._refresh_row(i, row_array[i])

    def _update_array(self, row_array):
        for index, r in enumerate(row_array):
            row_array[index] = r.ljust(self.charlcd.lcd.cols)
        self._refresh(row_array)

    def update(self, content):
        # split content into rows - row processing allows buffering and scrolling
        row_array = content.split('\n')
        # ensure array size is at least as big as lcd
        while len(row_array) < self.charlcd.lcd.rows:
            row_array.append('')
        self._update_array(row_array)

    @button
    def is_up(self):
        joyVal = self.joystick.is_up()
        return joyVal

    @button
    def is_down(self):
        joyVal = self.joystick.is_down()
        return joyVal

    @button
    def is_left(self):
        joyVal = self.joystick.is_left()
        return joyVal

    @button
    def is_right(self):
        joyVal = self.joystick.is_right()
        return joyVal

    @button
    def is_select(self):
        joyVal = self.joystick.is_select()
        return joyVal


class MenuRunner():
    def __init__(self, virtualui: VirtualUI = None, top_menu: MenuNav = None, *args, **kwargs):
        if not isinstance(virtualui, VirtualUI):
            raise ValueError('virtualui must be of type VirtualUI')
        if not isinstance(top_menu, MenuNav):
            raise ValueError('top_menu must be of type MenuNav')

        self.virtualui = virtualui
        self.top_menu = top_menu
        self._greenlet = None

    def start(self):
        self._greenlet = gevent.spawn(self._start)
        self._greenlet.start()

    def _start(self):
        self.virtualui.start()

        _logger.info('MenuRunner started: {}'.format(self))

        menu = self.top_menu

        while True:
            gevent.sleep(menu.refresh_ms / 1000)

            if self.virtualui.is_left():
                menu = menu.left_press()
            elif self.virtualui.is_right():
                menu = menu.right_press()
            elif self.virtualui.is_down():
                menu = menu.down_press()
            elif self.virtualui.is_up():
                menu = menu.up_press()
            elif self.virtualui.is_select():
                menu = menu.select_press()

            menu = menu.action()
            self.virtualui.update(menu.text)

    def stop(self):
        self.virtualui.stop()

        _logger.info('MenuRunner stopped: {}'.format(self))

        if self._greenlet:
            gevent.kill(self._greenlet)
