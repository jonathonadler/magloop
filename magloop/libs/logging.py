########################################################################
# Filename    : logging.py
# Description : logging helper
########################################################################

import logging
import logging.config
import os

from yaml import safe_load


def __log_trace(self, message, *args, **kwargs):
    self.log(logging.TRACE, message, *args, **kwargs)


def setup_app_logging(log_cfg_yaml_path='logging.yaml', default_level=logging.INFO):
    """
    Setup logging configuration
    see: https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
    see: https://gist.github.com/kingspp/9451566a5555fb022215ca2b7b802f19
    """

    filepath = log_cfg_yaml_path
    if os.path.exists(filepath):
        with open(filepath, 'rt') as f:
            try:
                log_config = safe_load(f.read())
                logging.config.dictConfig(log_config)
            except Exception as e:
                print(e)
                print('Error in logging configuration file. Using default configuration.')
                logging.basicConfig(level=default_level)
    else:
        logging.basicConfig()
        print('Failed to load configuration file. Using default configuration.')

    logging.root.setLevel(default_level)
    # logging.root.removeHandler(logging.root.handlers[0])


# add custom logging level (TRACE)
logging.TRACE = logging.DEBUG - 1
logging.addLevelName(logging.TRACE, 'TRACE')

logging.Logger.trace = __log_trace
